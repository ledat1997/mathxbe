<?php
use App\Enums\AlertIcon;
use App\Enums\UserStatus;
use App\Enums\UserRole;
use App\Enums\QuestionTypeStatus;
use App\Enums\QuestionStatus;
use App\Enums\TestType;
use App\Enums\TestStatus;

return [
    'alert' => [
        'icon' => [
            'success' => AlertIcon::Success,
            'error' => AlertIcon::Error,
            'info' => AlertIcon::Info,
            'warning' => AlertIcon::Warning
        ]
    ],
    'user' => [
        'status' => [
            UserStatus::Active => 'Active',
            UserStatus::Inactive => 'Inactive'
        ],
        'role' => [
            UserRole::Child => 'Children',
            UserRole::Parent => 'Parent'
        ]
    ],
    'question_type' => [
        'status' => [
            QuestionTypeStatus::Active => 'Active',
            QuestionTypeStatus::Inactive => 'Inactive'
        ],
    ],
    'question' => [
        'status' => [
            QuestionStatus::Active => 'Active',
            QuestionStatus::Inactive => 'Inactive'
        ],
    ],
    "grade" => [
        "6" => 6,
        "7" => 7,
        "8" => 8,
        "9" => 9,
        "10" => 10,
        "11" => 11,
        "12" => 12,
    ],
    "class" => [
        "1" => 1,
        "2" => 2,
        "3" => 3,
        "4" => 4,
        "5" => 5,
        "6" => 6,
        "7" => 7,
        "8" => 8,
        "9" => 9,
        "10" => 10,
        "11" => 11,
        "12" => 12,
    ],
    'year' => [
        "2020" => 2020,
        "2021" => 2021,
        "2022" => 2022,
        "2023" => 2023,
        "2024" => 2024,
        "2025" => 2025,
        "2026" => 2026,
        "2027" => 2027,
        "2028" => 2028,
        "2029" => 2029,
        "2030" => 2030,
        "2031" => 2031,
        "2032" => 2032,
        "2033" => 2033,
        "2034" => 2034,
        "2035" => 2035,
        "2036" => 2036,
        "2037" => 2037,
        "2038" => 2038,
        "2039" => 2039,
        "2040" => 2040,
        "2041" => 2041,
        "2042" => 2042,
        "2043" => 2043,
        "2044" => 2044,
        "2045" => 2045,
        "2046" => 2046,
        "2047" => 2047,
        "2048" => 2048,
        "2049" => 2049,
        "2050" => 2050,
        "2051" => 2051,
        "2052" => 2052,
        "2053" => 2053,
        "2054" => 2054,
        "2055" => 2055,
        "2056" => 2056,
        "2057" => 2057,
        "2058" => 2058,
        "2059" => 2059,
        "2060" => 2060,
        "2061" => 2061,
        "2062" => 2062,
        "2063" => 2063,
        "2064" => 2064,
        "2065" => 2065,
        "2066" => 2066,
        "2067" => 2067,
        "2068" => 2068,
        "2069" => 2069,
        "2070" => 2070,
    ],
    'test' => [
        'test_type' => [
            TestType::BaseTest => "Base Test",
            TestType::HourTest => "Hour Test",
            TestType::MidTermTest => "Midterm Test",
            TestType::FinalTermTest => "Final Test",
        ],
        'status' => [
            TestStatus::Active => 'Active',
            TestStatus::Inactive => 'Inactive',
        ]
    ]

];