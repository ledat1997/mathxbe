<?php

return  [
    [
        'name' => 'User',
        'route' => 'admin.user.*',
        'icon' => 'fa-solid fa-user',
        'children' => [
            [
                'name' => 'List User',
                'route' => 'admin.user.index',
                'icon' => 'fa-solid fa-list',
            ],
            [
                'name' => 'Create User',
                'route' => 'admin.user.create',
                'icon' => 'fa-solid fa-circle-plus',
            ],
        ]
    ],
    [
        'name' => 'Class Room',
        'route' => 'admin.class_room.*',
        'icon' => 'fa-solid fa-landmark',
        'children' => [
            [
                'name' => 'List class Room',
                'route' => 'admin.class_room.index',
                'icon' => 'fa-solid fa-list',
            ],
            [
                'name' => 'Create class Room',
                'route' => 'admin.class_room.create',
                'icon' => 'fa-solid fa-circle-plus',
            ],
        ]
    ],
    [
        'name' => 'Questions',
        'route' => 'admin.question.*',
        'icon' => 'fa-solid fa-circle-question',
        'children' => [
            [
                'name' => 'List questions',
                'route' => 'admin.question.index',
                'icon' => 'fa-solid fa-list',
            ],
            [
                'name' => 'Create question',
                'route' => 'admin.question.create',
                'icon' => 'fa-solid fa-circle-plus',
            ],
        ]
    ],
    [
        'name' => 'Test',
        'route' => 'admin.test.*',
        'icon' => 'fa-solid fa-file-lines',
        'children' => [
            [
                'name' => 'List test',
                'route' => 'admin.test.index',
                'icon' => 'fa-solid fa-list',
            ],
            [
                'name' => 'Create test',
                'route' => 'admin.test.create',
                'icon' => 'fa-solid fa-circle-plus',
            ],
        ]
    ],
    [
        'name' => 'Question type',
        'route' => 'admin.question_type.*',
        'icon' => 'fa-solid fa-file-circle-question',
        'children' => [
            [
                'name' => 'List question type',
                'route' => 'admin.question_type.index',
                'icon' => 'fa-solid fa-list',
            ],
            [
                'name' => 'Create question type',
                'route' => 'admin.question_type.create',
                'icon' => 'fa-solid fa-circle-plus',
            ],
        ]
    ],
    [
        'name' => 'Profile',
        'route' => 'admin.user.profile',
        'icon' => 'fa-solid fa-address-card',
    ],
    [
        'name' => 'Logout',
        'route' => 'admin.logout',
        'icon' => 'fa-solid fa-right-from-bracket',
    ],
];
