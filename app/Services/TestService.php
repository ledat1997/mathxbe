<?php

namespace App\Services;

use App\Repositories\Test\TestRepositoryInterface;

class TestService extends BaseService
{
    public function __construct(TestRepositoryInterface $repository)
    {
        parent::__construct($repository);
    }

    public function list()
    {
        return $this->repository->list();
    }
}