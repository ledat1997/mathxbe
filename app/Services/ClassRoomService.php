<?php

namespace App\Services;

use App\Repositories\ClassRoom\ClassRoomRepositoryInterface;

class ClassRoomService
{
    protected ClassRoomRepositoryInterface $classRoomRepository;

    public function __construct(ClassRoomRepositoryInterface $classRoomRepository)
    {
        $this->classRoomRepository = $classRoomRepository;
    }

    public function list($dataSearch)
    {
        return $this->classRoomRepository->list($dataSearch);
    }

    public function create($data)
    {
        $data['name'] = $data['grade'] . '/' . $data['class'];
        return $this->classRoomRepository->create($data);
    }

    public function get($id)
    {
        return $this->classRoomRepository->findById($id);
    }

    public function update($id, $data)
    {
        $data['name'] = $data['grade'] . '/' . $data['class'];
        return $this->classRoomRepository->update($id, $data);
    }

    public function delete($id)
    {
        return $this->classRoomRepository->delete($id);
    }
}