<?php

namespace App\Services;

use App\Enums\UserRole;
use App\Enums\UserStatus;
use App\Repositories\ClassRoom\ClassRoomRepositoryInterface;
use App\Repositories\ParentUser\ParentUserRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserService
{
    protected $userRepository;

    protected $parentUserRepository;

    protected $classRoomRepository;

    public function __construct(
        UserRepositoryInterface $userRepository,
        ParentUserRepositoryInterface $parentUserRepository,
        ClassRoomRepositoryInterface $classRoomRepository
    ) {
        $this->userRepository = $userRepository;
        $this->parentUserRepository = $parentUserRepository;
        $this->classRoomRepository = $classRoomRepository;
    }

    public function handleLogin($data)
    {
        $credentials = [
            'email' => $data['email'],
            'password' => $data['password'],
            'status' => UserStatus::Active,
            'role' => UserRole::Admin
        ];
        $remember = isset($data['remember']);
        if (Auth::attempt($credentials, $remember)) {
            return true;
        }
        return false;
    }

    public function list($dataSearch)
    {
        return $this->userRepository->list($dataSearch);
    }

    public function delete($id)
    {
        $user = $this->userRepository->findById($id);
        DB::beginTransaction();
        try {
            $user->parentUsers()->delete();
            $user->delete();
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            logger($e->getFile() . ':' . $e->getLine() . ':' . $e->getMessage());
            return false;
        }

    }

    public function create($data)
    {
        $data['password'] = Hash::make('password');
        DB::beginTransaction();
        try {
            $user = $this->userRepository->create($data);
            if (isset($data['users'])) {
                $parentUser = [];
                foreach ($data['users'] as $item) {
                    array_push($parentUser, [
                        'parent_id' => $user->id,
                        'user_id' => $item
                    ]);
                }
                $this->parentUserRepository->insert($parentUser);
            }
            DB::commit();
            return true;
        } catch(\Exception $e) {
            DB::rollBack();
            logger($e->getFile() . ':' . $e->getLine() . ':' . $e->getMessage());
            return false;
        }
    }

    public function getListChildren($userId = null)
    {
        return $this->userRepository->findListChildren($userId);
    }

    public function getUser($id)
    {
        $user = $this->userRepository->getInfoById($id);
        $user->child = $user->child->map(function($item) {
            return $item->id;
        });
        return $user;
    }

    public function updateUser($data, $id)
    {
        $userParents = $this->parentUserRepository->find(['parent_id' => $id])->pluck('user_id')->toArray();
        $data['users'] = $data['users'] ?? [];
        $userDelete = array_diff($userParents, $data['users']);
        $userAdd = array_diff($data['users'], $userParents);
        DB::beginTransaction();
        try {
            $this->parentUserRepository->find(['parent_id' => $id]);
            if (count($userAdd)) {
                $parentUserAdd = [];
                foreach ($userAdd as $item) {
                    array_push($parentUserAdd, [
                        'parent_id' => $id,
                        'user_id' => $item
                    ]);
                }
                $this->parentUserRepository->insert($parentUserAdd);
            }
            if (count($userDelete)) {
                $parentUserDelete = [];
                foreach ($userDelete as $item) {
                    array_push($parentUserDelete, [
                        'parent_id' => $id,
                        'user_id' => $item
                    ]);
                }
                $this->parentUserRepository->deleteByConditions($parentUserDelete);
            }
            $this->userRepository->update($id, $data);
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            logger($e->getFile() . ':' . $e->getLine() . ':' . $e->getMessage());
            return false;
        }
    }

    public function showInfo($id)
    {
        $year = getYear();
        $classRoom = $this->classRoomRepository->getClassInYear($year)->pluck('nameTeacher', 'id');
        $column = ['full_name', 'email', 'phone', 'address', 'role', 'status', 'id'];
        $user = $this->userRepository->getInfoDetailById($id, $column);
//        dd($user);
        return [$user, $classRoom, $year];
    }
}