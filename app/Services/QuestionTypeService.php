<?php

namespace App\Services;

use App\Enums\QuestionTypeStatus;
use App\Repositories\QuestionType\QuestionTypeRepositoryInterface;

class QuestionTypeService
{
    protected QuestionTypeRepositoryInterface $questionTypeRepository;

    public function __construct(QuestionTypeRepositoryInterface $questionTypeRepository)
    {
        $this->questionTypeRepository = $questionTypeRepository;
    }

    public function list($dataSearch)
    {
        return $this->questionTypeRepository->list($dataSearch);
    }

    public function create($data)
    {
        return $this->questionTypeRepository->create($data);
    }

    public function get($id)
    {
        return $this->questionTypeRepository->findById($id);
    }

    public function update($id, $data)
    {
        return $this->questionTypeRepository->update($id, $data);
    }

    public function delete($id)
    {
        return $this->questionTypeRepository->delete($id);
    }

    public function getItemActive()
    {
        return $this->questionTypeRepository->find(['status' => QuestionTypeStatus::Active]);
    }
}