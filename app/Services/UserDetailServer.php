<?php

namespace App\Services;

use App\Repositories\UserDetail\UserDetailRepositoryInterface;

class UserDetailServer
{
    protected $userDetailRepository;

    public function __construct(UserDetailRepositoryInterface $userDetailRepository)
    {
        $this->userDetailRepository = $userDetailRepository;
    }

    public function create($data)
    {
        $data['start_year'] = getYear();
        $data['end_year'] = getYear() + 1;
        $data['status'] = 0;
        return $this->userDetailRepository->create($data);
    }

    public function update($data, $id)
    {
        return $this->userDetailRepository->update($id, $data);
    }
}