<?php

namespace App\Services;

use App\Enums\AnswerStatus;
use App\Repositories\Answer\AnswerRepositoryInterface;
use App\Repositories\Question\QuestionRepositoryInterface;
use Illuminate\Support\Facades\DB;

class QuestionService extends BaseService
{
    protected $answerRepository;

    public function __construct(QuestionRepositoryInterface $questionRepository, AnswerRepositoryInterface $answerRepository)
    {
        parent::__construct($questionRepository);
        $this->answerRepository = $answerRepository;
    }

    public function list($dataSearch)
    {
        return $this->repository->list($dataSearch);
    }

    public function create($data)
    {
        DB::beginTransaction();
        try {
            $data['answer_correct'] = $this->getNumOfCorrectQuestion($data['answers']);
            $question = $this->repository->create($data);
            $answersCreate = array_map(function($item) use ($question) {
                $item['question_id'] = $question->id;
                $item['created_at'] = now();
                $item['updated_at'] = now();
                unset($item['id']);
                return $item;
            }, $data['answers']);
            $this->answerRepository->insert($answersCreate);
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            logger($e->getFile() . ':' . $e->getLine() . ':' . $e->getMessage());
            return false;
        }
    }

    public function getById($questionId)
    {
        return $this->repository->getById($questionId);
    }

    public function updateById($data, $id)
    {
        $data['answer_correct'] = $this->getNumOfCorrectQuestion($data['answers']);
        $answerIdsData = array_filter(array_column($data['answers'], 'id'));
        $listAnswerQuestion = $this->answerRepository->find(['question_id' => $id])->pluck('id')->toArray();
        $answerIdsDelete = array_diff($listAnswerQuestion, $answerIdsData);
        $dataCreate = array_map(function($item) use ($id) {
            $item['question_id'] = $id;
            return $item;
        }, $data['answers']);
        DB::beginTransaction();
        try {
            foreach ($dataCreate as $answer) {
                if (is_null($answer['id'])) {
                    $this->answerRepository->create($answer);
                } else {
                    $this->answerRepository->update($answer['id'], $answer);
                }
            }
            $this->answerRepository->deleteAnswerByIds($answerIdsDelete);
            $this->repository->update($id, $data);
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            logger($e->getFile() . ':' . $e->getLine() . ':' . $e->getMessage());
            return false;
        }
    }

    public function delete($id)
    {
        $question = $this->repository->findById($id);
        DB::beginTransaction();
        try {
            $question->answers()->delete();
            $question->delete();
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            logger($e->getFile() . ':' . $e->getLine() . ':' . $e->getMessage());
            return false;
        }
    }

    private function getNumOfCorrectQuestion($answers)
    {
        $answerCorrect = array_filter($answers, function($item) {
            return $item['status'] == AnswerStatus::Correct;
        });
        return count($answerCorrect);
    }
}