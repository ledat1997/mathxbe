<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'total',
        'detail',
        'floor',
        'total_time',
        'start_time',
        'finish_time',
        'class_test',
        'test_type',
        'time_test'
    ];
}
