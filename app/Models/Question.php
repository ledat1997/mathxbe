<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'question_type_id',
        'status',
        'note',
        'avatar',
        'images'
    ];

    public function questionType()
    {
         return $this->belongsTo(QuestionType::class, 'question_type_id', 'id');
    }

    public function answers()
    {
        return $this->hasMany(Answer::class, 'question_id', 'id');
    }
}
