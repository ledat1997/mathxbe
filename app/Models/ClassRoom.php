<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassRoom extends Model
{
    use HasFactory;

    protected $fillable = [
        'grade',
        'class',
        'teacher',
        'description',
        'year'
    ];

    public function getNameAttribute()
    {
        return $this->grade . "/" . $this->class;
    }

    public function getNameTeacherAttribute()
    {
        return $this->grade . "/" . $this->class . " - " . $this->teacher;
    }
}
