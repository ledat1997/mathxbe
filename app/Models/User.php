<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Enums\UserRole;
use App\Enums\UserStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'full_name',
        'email',
        'password',
        'email_verified_at',
        'remember_token',
        'address',
        'phone',
        'status',
        'role',
        'birthday',
        'avatar'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'email_verified_at' => 'datetime',
            'password' => 'hashed',
        ];
    }

    public function child()
    {
        return $this->belongsToMany(User::class, 'parent_users', 'parent_id', 'user_id');
    }

    public function parent()
    {
        return $this->belongsToMany(User::class, 'parent_users', 'user_id', 'parent_id');
    }

    public function parentUsers()
    {
        return $this->hasMany(ParentUser::class, 'parent_id', 'id');
    }

    public function userDetails()
    {
        return $this->hasMany(UserDetail::class, 'user_id', 'id');
    }
}
