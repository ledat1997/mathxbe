<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class CheckAnswerCorrect implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $valid = false;
        if (is_array($value)) {
            foreach ($value as $item) {
                if (isset($item['status']) && $item['status'] == 1) {
                    $valid = true;
                }
            }
        }
        if (!$valid) {
            $fail('The :attribute must at least one correct answer.');
        }
    }
}
