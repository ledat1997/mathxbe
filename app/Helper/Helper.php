<?php

if (!function_exists('getFullNameChild')) {
    function getFullNameChild($data) {
        $result = '';
        foreach ($data as $index => $item) {
            if ($index) $result .= ', ';
            $result .= $item;
        }
        return $result;
    }
}

if(!function_exists('getYear')) {
    function getYear()
    {
        $year = date('Y');
        $currentDate = date('Y-m-d');
        $targetDate = $year . '-06-01';
        if ($currentDate < $targetDate) {
            return $year - 1;
        }
        return $year;

    }
}