<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LoginRequest;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function formLogin()
    {
        return view('pages.admin.auth.login');
    }

    public function login(LoginRequest $request)
    {
        $result = $this->userService->handleLogin($request->only(['email', 'password', 'remember']));
        if($result) {
            return redirect()->route('admin.user.index');
        }
        return redirect()->back();

    }

    public function profile()
    {
        return 1;
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('admin.auth.login.form');
    }
}
