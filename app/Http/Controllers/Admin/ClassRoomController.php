<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ClassRoomRequest;
use App\Services\ClassRoomService;
use Illuminate\Http\Request;

class ClassRoomController extends Controller
{
    protected $classRoomService;

    public function __construct(ClassRoomService $classRoomService)
    {
        $this->classRoomService = $classRoomService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $classes = $this->classRoomService->list($request->all());
        return view('pages.admin.classroom.index', ['classes' => $classes]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view("pages.admin.classroom.create");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ClassRoomRequest $request)
    {
        $this->classRoomService->create($request->all());
        return redirect()->route('admin.class_room.index')->with(['type'=> 'success', 'message' => trans("classroom.result_action.create_success")]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $classRoom = $this->classRoomService->get($id);
        return view('pages.admin.classroom.edit', ['classroom' => $classRoom]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ClassRoomRequest $request, string $id)
    {
        $this->classRoomService->update($id, $request->all());
        return redirect()->route('admin.class_room.index')->with(['type'=> 'success', 'message' => trans("classroom.result_action.update_success")]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->classRoomService->delete($id);
        return redirect()->route('admin.class_room.index')->with(['type'=> 'success', 'message' => trans("classroom.result_action.delete_success")]);
    }
}
