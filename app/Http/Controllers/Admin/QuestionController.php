<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\QuestionRequest;
use App\Services\QuestionService;
use App\Services\QuestionTypeService;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    protected $questionService;

    protected $questionTypeService;

    public function __construct(QuestionService $questionService, QuestionTypeService $questionTypeService)
    {
        $this->questionService = $questionService;
        $this->questionTypeService = $questionTypeService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $questions = $this->questionService->list($request->all());
        $questionTypes = $this->questionTypeService->getItemActive()->pluck('title', 'id');
        return view('pages.admin.question.index', ['questions' => $questions, 'questionTypes' => $questionTypes]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $questionTypes = $this->questionTypeService->getItemActive()->pluck('title', 'id');
        return view('pages.admin.question.create',['questionTypes' => $questionTypes]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(QuestionRequest $request)
    {
        $result = $this->questionService->create($request->all());
        return $result
            ? redirect()->route('admin.question.index')->with(['type' => 'success', 'message' => trans('question.result_action.create_success')])
            : redirect()->back()->with(['type' => 'error', 'message' => trans('question.result_action.create_fail')]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $question = $this->questionService->getById($id);
        $questionTypes = $this->questionTypeService->getItemActive()->pluck('title', 'id');
        return view('pages.admin.question.edit',['questionTypes' => $questionTypes, 'question' => $question]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(QuestionRequest $request, string $id)
    {
        $result = $this->questionService->updateById($request->all(), $id);
        return $result
            ? redirect()->route('admin.question.index')->with(['type' => 'success', 'message' => trans('question.result_action.update_success')])
            : redirect()->back()->with(['type' => 'error', 'message' => trans('question.result_action.update_fail')]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $result = $this->questionService->delete($id);
        return $result
            ? redirect()->route('admin.question.index')->with(['type' => 'success', 'message' => trans('question.result_action.delete_success')])
            : redirect()->back()->with(['type' => 'error', 'message' => trans('question.result_action.delete_fail')]);
    }
}
