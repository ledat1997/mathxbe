<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\QuestionTypeRequest;
use App\Services\QuestionTypeService;
use Illuminate\Http\Request;

class QuestionTypeController extends Controller
{
    protected QuestionTypeService $questionTypeService;

    public function __construct(QuestionTypeService $questionTypeService)
    {
        $this->questionTypeService = $questionTypeService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $questionTypes = $this->questionTypeService->list($request->all());
        return view('pages.admin.question_type.index', ['questionTypes' => $questionTypes]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view("pages.admin.question_type.create");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(QuestionTypeRequest $request)
    {
        $this->questionTypeService->create($request->all());
        return redirect()->route('admin.question_type.index')->with(['type'=> 'success', 'message' => trans("question_type.result_action.create_success")]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $questionType = $this->questionTypeService->get($id);
        return view('pages.admin.question_type.edit', ['questionType' => $questionType]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(QuestionTypeRequest $request, string $id)
    {
        $this->questionTypeService->update($id, $request->all());
        return redirect()->route('admin.question_type.index')->with(['type'=> 'success', 'message' => trans("question_type.result_action.update_success")]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->questionTypeService->delete($id);
        return redirect()->route('admin.question_type.index')->with(['type'=> 'success', 'message' => trans("question_type.result_action.delete_success")]);
    }
}
