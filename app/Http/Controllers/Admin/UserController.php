<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserRequest;
use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $users = $this->userService->list($request->all());
        return view('pages.admin.user.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $listChildren = $this->userService->getListChildren();
        return view('pages.admin.user.create', ['listChildren' => $listChildren]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(UserRequest $request)
    {
        $result = $this->userService->create($request->except('_token'));
        return $result
            ? redirect()->route('admin.user.index')->with(['type' => 'success', 'message' => trans('user.result_action.create_success')])
            : redirect()->back()->with(['type' => 'error', 'message' => trans('user.result_action.create_fail')]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        list($user, $classroom) = $this->userService->showInfo($id);
        return view('pages.admin.user.show', ['user' => $user, 'classroom' => $classroom]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $listChildren = $this->userService->getListChildren($id);
        $user = $this->userService->getUser($id);
        return view('pages.admin.user.edit', ['user' => $user, 'listChildren' => $listChildren]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UserRequest $request, $id)
    {
        $this->userService->updateUser($request->except('_token'), $id);
        return redirect()->route('admin.user.index')->with(['type' => 'success', 'message' => trans('user.result_action.update_success')]);;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $result = $this->userService->delete($id);
        return $result
            ? redirect()->back()->with(['type' => 'success', 'message' => trans('user.result_action.delete_success')])
            : redirect()->back()->with(['type' => 'error', 'message' => trans('user.result_action.delete_fail')]);
    }
}

