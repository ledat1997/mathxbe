<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\UserDetailServer;
use Illuminate\Http\Request;

class UserDetailController extends Controller
{
    protected $userDetailServer;

    public function __construct(UserDetailServer $userDetailServer)
    {
        $this->userDetailServer = $userDetailServer;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->userDetailServer->create($request->all());
        return redirect()->back()->with(['type' => 'success', 'message' => 'Thao tác thành công']);
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $this->userDetailServer->update($request->all(), $id);
        return redirect()->back()->with(['type' => 'success', 'message' => 'Thao tác thành công']);
    }
}
