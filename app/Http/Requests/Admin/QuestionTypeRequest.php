<?php

namespace App\Http\Requests\Admin;

use App\Enums\QuestionTypeStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class QuestionTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required|max:50',
            'status' => [
                'required',
                Rule::in([QuestionTypeStatus::Inactive, QuestionTypeStatus::Active])
            ],
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'status' => isset($this->status) ? QuestionTypeStatus::Active : QuestionTypeStatus::Inactive,
        ]);
    }
}
