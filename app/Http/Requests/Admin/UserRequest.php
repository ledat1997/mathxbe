<?php

namespace App\Http\Requests\Admin;

use App\Enums\UserRole;
use App\Enums\UserStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'email' => 'required|unique:users,id|max:255',
            'full_name' => 'required|max:200',
            'phone' => 'required|min:10|max:11',
            'address' => 'required|max:200',
            'role' => [
                'required',
                Rule::in([UserRole::Child, UserRole::Parent])
            ],
            'status' => [
                'required',
                Rule::in([UserStatus::Inactive, UserStatus::Active])
            ],
            'users' => 'required_if:role,' . UserRole::Parent
        ];
        if ($this->isMethod('post')) {
            $rules['email'] = 'required|unique:users|max:255';
        }

        return $rules;
    }

    public function prepareForValidation()
    {
        $this->merge([
            'role' => isset($this->role) ? UserRole::Parent : UserRole::Child,
            'status' => isset($this->status) ? UserStatus::Active : UserStatus::Inactive,
        ]);
    }

    public function messages()
    {
        return [
            'users.required_if' => trans('user.user.validation.required')
        ];
    }
}
