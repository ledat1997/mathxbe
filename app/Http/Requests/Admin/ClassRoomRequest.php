<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ClassRoomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'grade' => [
                'required',
                Rule::in(config('constants.grade'))
            ],
            'class' => [
                'required',
                Rule::in(config('constants.class'))
            ],
            'teacher' => 'max:50',
            'year' => [
                'required',
                Rule::in(config('constants.year')),
                Rule::unique('class_rooms')->where(function ($query) {
                    return $query->where('class', $this->class)
                        ->where('grade', $this->grade);;
                }),
            ],
        ];
    }
}
