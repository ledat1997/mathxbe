<?php

namespace App\Http\Requests\Admin;

use App\Enums\AnswerStatus;
use App\Enums\QuestionStatus;
use App\Models\Answer;
use App\Models\QuestionType;
use App\Rules\CheckAnswerCorrect;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class QuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $questionTypeIds = QuestionType::all('id')->pluck('id');
        return [
            'title' => 'required',
            'question_type_id' => [
                'required',
                Rule::in($questionTypeIds)
            ],
            'status' => 'required',
            'answers' => ['required', 'array', new CheckAnswerCorrect()],
            'answers.*.answer' => 'required',
            'answers.*.status' => 'required'
        ];
    }

    public function prepareForValidation()
    {
        $answers = array_map(function($item) {
            $item['status'] = isset($item['status']) ? AnswerStatus::Correct : AnswerStatus::Wrong;
            return $item;
        }, $this->answers);
        return $this->merge([
            'answers' => $answers,
            'status' => isset($this->status) ? QuestionStatus::Active : QuestionStatus::Inactive
        ]);
    }

    public function messages()
    {
        return [
            'answers.*.answer.required' => 'This field is required.'
        ];
    }
}
