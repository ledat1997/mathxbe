<?php

namespace App\Providers;

use App\Services\QuestionService;
use App\Services\TestService;
use Illuminate\Support\ServiceProvider;

class ServiceServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(QuestionService::class);
        $this->app->singleton(TestService::class);
    }

    public function boot()
    {

    }
}
