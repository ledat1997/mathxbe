<?php

namespace App\Providers;

use App\Repositories\Answer\AnswerRepository;
use App\Repositories\Answer\AnswerRepositoryInterface;
use App\Repositories\BaseRepository;
use App\Repositories\BaseRepositoryInterface;
use App\Repositories\ClassRoom\ClassRoomRepository;
use App\Repositories\ClassRoom\ClassRoomRepositoryInterface;
use App\Repositories\ParentUser\ParentUserRepository;
use App\Repositories\ParentUser\ParentUserRepositoryInterface;
use App\Repositories\Question\QuestionRepository;
use App\Repositories\Question\QuestionRepositoryInterface;
use App\Repositories\QuestionType\QuestionTypeRepository;
use App\Repositories\QuestionType\QuestionTypeRepositoryInterface;
use App\Repositories\Test\TestRepository;
use App\Repositories\Test\TestRepositoryInterface;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\UserDetail\UserDetailRepository;
use App\Repositories\UserDetail\UserDetailRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(BaseRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(ClassRoomRepositoryInterface::class, ClassRoomRepository::class);
        $this->app->bind(ParentUserRepositoryInterface::class, ParentUserRepository::class);
        $this->app->bind(UserDetailRepositoryInterface::class, UserDetailRepository::class);
        $this->app->bind(QuestionTypeRepositoryInterface::class, QuestionTypeRepository::class);
        $this->app->bind(QuestionRepositoryInterface::class, QuestionRepository::class);
        $this->app->bind(AnswerRepositoryInterface::class, AnswerRepository::class);
        $this->app->bind(TestRepositoryInterface::class, TestRepository::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
