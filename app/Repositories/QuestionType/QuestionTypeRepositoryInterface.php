<?php

namespace App\Repositories\QuestionType;

use App\Repositories\BaseRepositoryInterface;

interface QuestionTypeRepositoryInterface extends BaseRepositoryInterface
{
    public function list($dataSearch);
}