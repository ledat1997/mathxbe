<?php

namespace App\Repositories\QuestionType;

use App\Models\QuestionType;
use App\Repositories\BaseRepository;

class QuestionTypeRepository extends BaseRepository implements QuestionTypeRepositoryInterface
{
    public function __construct(QuestionType $questionType)
    {
        parent::__construct($questionType);
        $this->paginate = 20;
    }

    public function list($dataSearch)
    {
        return $this->model
            ->when(isset($dataSearch['title']), function($query) use ($dataSearch) {
                return $query->where('title', 'like', "%" . trim($dataSearch["title"]) . "%");
            })
            ->when(isset($dataSearch['status']), function($query) use ($dataSearch) {
                return $query->where('status', $dataSearch["status"]);
            })
            ->orderBy('updated_at', 'desc')
            ->paginate($this->paginate)
            ->withQueryString();
    }
}