<?php
namespace App\Repositories\ClassRoom;

use App\Repositories\BaseRepositoryInterface;

interface ClassRoomRepositoryInterface extends BaseRepositoryInterface
{

    public function list($dataSearch);

    public function getClassInYear($year, $select);
}