<?php

namespace App\Repositories\ClassRoom;

use App\Models\ClassRoom;
use App\Repositories\BaseRepository;

class ClassRoomRepository extends BaseRepository implements ClassRoomRepositoryInterface
{

    public function __construct(ClassRoom $classRoom)
    {
        parent::__construct($classRoom);
        $this->paginate = 20;
    }

    public function list($dataSearch)
    {
        return $this->model
            ->when(isset($dataSearch['grade']), function($query) use ($dataSearch) {
                return $query->where('grade', $dataSearch["grade"]);
            })
            ->when(isset($dataSearch['class']), function($query) use ($dataSearch) {
                return $query->where('class', $dataSearch["class"]);
            })
            ->when(isset($dataSearch['teacher']), function($query) use ($dataSearch) {
                return $query->where('teacher', 'like', "%" . trim($dataSearch["teacher"]) . "%");
            })
            ->when(isset($dataSearch['year']), function($query) use ($dataSearch) {
                return $query->where('year', $dataSearch["year"]);
            })
            ->orderBy('updated_at', 'desc')
            ->paginate($this->paginate)
            ->withQueryString();
    }

    public function getClassInYear($year, $select=["*"])
    {
        return $this->model->select($select)->where('year', $year)->get();
    }
}