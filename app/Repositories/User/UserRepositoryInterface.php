<?php

namespace App\Repositories\User;

use App\Repositories\BaseRepositoryInterface;

interface UserRepositoryInterface extends BaseRepositoryInterface
{
    public function list($dataSearch);

    public function findListChildren($excludeId);

    public function getInfoById($userId);

    public function getInfoDetailById($userId, $select=["*"]);
}