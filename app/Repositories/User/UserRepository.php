<?php

namespace App\Repositories\User;

use App\Enums\UserRole;
use App\Models\User;
use App\Repositories\BaseRepository;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->paginate = 20;
    }

    public function list($dataSearch)
    {
        return $this->model
            ->with('child')
            ->whereNot('role', UserRole::Admin)
            ->when(isset($dataSearch['name']), function($query) use ($dataSearch) {
                return $query->where('full_name', 'like', "%" . trim($dataSearch["name"]) . "%");
            })
            ->when(isset($dataSearch['status']), function($query) use ($dataSearch) {
                return $query->where('status', $dataSearch['status']);
            })
            ->when(isset($dataSearch['role']), function($query) use ($dataSearch) {
                return $query->where('role', $dataSearch['role']);
            })
            ->orderBy('updated_at', 'desc')
            ->paginate($this->paginate)
            ->withQueryString();
    }

    public function findListChildren($excludeId)
    {
        return $this->model->where('role', UserRole::Child)->whereNot('id', $excludeId)->get()->pluck('full_name', 'id');
    }

    public function getInfoById($userId, $select=["*"])
    {
        return $this->model->with('child')->select($select)->findOrFail($userId);
    }

    public function getInfoDetailById($userId, $select=["*"])
    {
        return $this->model->with(['userDetails', 'userDetails.classRoom', 'child' => function($item) {
            return $item->with(['userDetails', 'userDetails.classRoom']);
        }])->select($select)->findOrFail($userId);
    }
}