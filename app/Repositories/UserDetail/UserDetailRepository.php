<?php

namespace App\Repositories\UserDetail;

use App\Models\UserDetail;
use App\Repositories\BaseRepository;

class UserDetailRepository extends BaseRepository implements UserDetailRepositoryInterface
{
    public function __construct(UserDetail $userDetail)
    {
        parent::__construct($userDetail);
    }
}