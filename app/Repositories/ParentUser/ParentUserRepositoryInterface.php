<?php

namespace App\Repositories\ParentUser;

use App\Repositories\BaseRepositoryInterface;

interface ParentUserRepositoryInterface extends BaseRepositoryInterface
{
    public function deleteByConditions($conditions);
}