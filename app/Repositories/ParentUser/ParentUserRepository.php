<?php

namespace App\Repositories\ParentUser;

use App\Models\ParentUser;
use App\Repositories\BaseRepository;

class ParentUserRepository extends BaseRepository implements ParentUserRepositoryInterface
{
    public function __construct(ParentUser $parentUser)
    {
        parent::__construct($parentUser);
    }

    public function deleteByConditions($conditions)
    {
        $query = $this->model->newQuery();
        $query->where(function ($q) use ($conditions) {
            foreach ($conditions as $condition) {
                $q->orWhere(function ($subQuery) use ($condition) {
                    foreach ($condition as $field => $value) {
                        $subQuery->where($field, $value);
                    }
                });
            }
        });
        return $query->delete();
    }
}