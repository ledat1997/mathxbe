<?php

namespace App\Repositories\Answer;

use App\Models\Answer;
use App\Repositories\BaseRepository;

class AnswerRepository extends BaseRepository implements AnswerRepositoryInterface
{
    public function __construct(Answer $answer)
    {
        parent::__construct($answer);
        $this->paginate = 20;
    }

    public function deleteAnswerByIds($answerIds)
    {
        return $this->model->whereIn('id', $answerIds)->delete();
    }
}