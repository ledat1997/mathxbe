<?php

namespace App\Repositories\Answer;

use App\Repositories\BaseRepositoryInterface;

interface AnswerRepositoryInterface extends BaseRepositoryInterface
{
    public function deleteAnswerByIds($answerIds);
}