<?php

namespace App\Repositories;

use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\BaseRepositoryInterface;
use Illuminate\Database\Query\Builder;

/**
 * Class AbstractRepository
 *
 * @package Kenini\Repository
 */
class BaseRepository implements BaseRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;
    protected $paginate;

    /**
     * AbstractRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->paginate = 20;
    }

    /**
     * @inheritdoc
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * @inheritdoc
     */
    public function find(array $conditions = [])
    {
        return $this->model->where($conditions)->get();
    }

    /**
     * @inheritdoc
     */
    public function findOne(array $conditions)
    {
        return $this->model->where($conditions)->first();
    }

    /**
     * @inheritdoc
     */
    public function findById(int $id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @inheritdoc
     */
    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    public function createMany(array $attributes)
    {
        $createdAttributes = [];
        foreach ($attributes as $attribute) {
            $createdAttributes[] = $this->model->create($attribute);
        }
        return $createdAttributes;
    }

    public function insert(array $attributes)
    {
        return $this->model->insert($attributes);
    }

    /**
     * @inheritdoc
     */
    public function update($id, array $attributes = [])
    {
        $record = $this->findById($id);
        return $record->update($attributes);
    }

    /**
     * @inheritdoc
     */
    public function save(Model $model)
    {
        return $model->save();
    }

    /**
     * @inheritdoc
     */
    public function delete($id)
    {
        $record = $this->findById($id);
        return $record->delete();
    }

    /**
     * @param $conditons
     * @param string $column
     * @param string $order
     * @return mixed
     */
    public function allWithPaginate($conditons, $column = 'id', $order = 'desc')
    {
        return $this->model->where($conditons)->orderBy($column, $order)->paginate($this->paginate)->withQueryString();
    }
}