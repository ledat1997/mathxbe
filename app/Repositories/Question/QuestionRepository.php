<?php

namespace App\Repositories\Question;

use App\Models\Question;
use App\Repositories\BaseRepository;

class QuestionRepository extends BaseRepository implements QuestionRepositoryInterface
{
    public function __construct(Question $question)
    {
        parent::__construct($question);
        $this->model = $question;
        $this->paginate = 20;
    }

    public function list($dataSearch)
    {
        return $this->model
            ->with('questionType')
            ->when(isset($dataSearch['title']), function($query) use ($dataSearch) {
                return $query->where('title', 'like', "%" . trim($dataSearch["title"]) . "%");
            })
            ->when(isset($dataSearch['status']), function($query) use ($dataSearch) {
                return $query->where('status', $dataSearch["status"]);
            })
            ->when(isset($dataSearch['question_type_id']), function($query) use ($dataSearch) {
                return $query->where('question_type_id', $dataSearch["question_type_id"]);
            })
            ->orderBy('updated_at', 'desc')
            ->paginate($this->paginate)
            ->withQueryString();
    }

    public function getById($questionId)
    {
        return $this->model->with('answers')->findOrFail($questionId);
    }
}