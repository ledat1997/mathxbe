<?php

namespace App\Repositories\Test;

use App\Models\Test;
use App\Repositories\BaseRepository;

class TestRepository extends BaseRepository implements TestRepositoryInterface
{
    public function __construct(Test $test)
    {
        parent::__construct($test);
        $this->paginate = 20;
    }

    public function list()
    {
        return $this->model->paginate($this->paginate)->withQueryString();
    }
}