<?php

namespace App\Repositories\Test;

use App\Repositories\BaseRepositoryInterface;

interface TestRepositoryInterface extends BaseRepositoryInterface
{
    public function list();
}