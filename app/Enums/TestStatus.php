<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Inactive()
 * @method static static Active()
 */
final class TestStatus extends Enum
{
    const Inactive = 0;
    const Active = 1;
}
