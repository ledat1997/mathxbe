<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Success()
 * @method static static Info()
 * @method static static Warning()
 * @method static static Error()
 */
final class AlertIcon extends Enum
{
    const Success = 'fa-check';
    const Info = 'fa-info';
    const Warning = 'fa-exclamation-triangle';
    const Error = 'fa-ban';
}
