<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static BaseTest()
 * @method static static HourTest()
 * @method static static MidTermTest()
 * @method static static FinalTermTest()
 */
final class TestType extends Enum
{
    const BaseTest = 1;
    const HourTest = 2;
    const MidTermTest = 3;
    const FinalTermTest = 4;
}
