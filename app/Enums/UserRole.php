<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Child()
 * @method static static Parent()
 * @method static static Admin()
 */
final class UserRole extends Enum
{
    const Child = 1;
    const Parent = 2;
    const Admin = 3;
}
