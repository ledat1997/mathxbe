<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Wrong()
 * @method static static Correct()
 */
final class AnswerStatus extends Enum
{
    const Wrong = 0;
    const Correct = 1;
}
