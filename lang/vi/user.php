<?php

return [
    "page" => [
        "list" => "List User",
        "create" => "Create User",
        "edit" => "Edit User",
        "show" => "Detail User",
    ],
    "title" => [
        "user_info" => "User info",
        "user_detail_study" => "User detail study",
        "user_children" => "User children"
    ],
    "full_name" => [
        "label" => "Full Name",
        "placeholder" => "Fill the full Name",
        "validation" => [
            "required" => "",
        ]
    ],
    "email" => [
        "label" => "Email",
        "placeholder" => "Fill the email",
        "validation" => [
            ""
        ]
    ],
    "phone" => [
        "label" => "Phone",
        "placeholder" => "Fill the phone",
        "validation" => [
            ""
        ]
    ],
    "address" => [
        "label" => "Address",
        "placeholder" => "Fill the address",
        "validation" => [
            ""
        ]
    ],
    "role" => [
        "label" => "Role",
        "value" => [
            "on" => "Parent",
            "off" => "student"
        ],
        "validation" => [
            ""
        ]
    ],
    "status" => [
        "label" => "Status",
        "value" => [
            "on" => "active",
            "off" => "inactive"
        ],
        "validation" => [
            ""
        ]
    ],
    "user" => [
        "label" => "Users",
        "validation" => [
            "required" => "The users field is required"
        ]
    ],
    "classroom" => [
        "label" => "Class Room",
        "text_default" => "Fill class room"
    ],
    "description" => [
        "label" => "Description",
        "placeholder" => "Fill Placeholder"
    ],
    "point_average" => [
        "label" => "Point average"
    ],
    "result_action" => [
        "create_success" => "Create new user success",
        "create_fail" => "Create new user fail",
        "update_success" => "Update user success",
        "update_fail" => "Update user fail",
        "delete_success" => "Delete user success",
        "delete_fail" => "Delete user fail",
    ]
];