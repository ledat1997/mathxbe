<?php

return [
    'page' => [
        'list' => 'List question type',
        'create' => 'Create question type',
        'edit' => 'Edit question type'
    ],
    'title' => [
        'label' => 'Title',
        'placeholder' => 'Fill the title',
        'validation' => [
            'required' => '',
        ]
    ],
    'description' => [
        'label' => 'Description',
        'placeholder' => 'Fill the description',
        'validation' => [
            'required' => '',
        ]
    ],
    "status" => [
        "label" => "Status",
        "value" => [
            "on" => "active",
            "off" => "inactive"
        ],
        "validation" => [
            ""
        ]
    ],
    'result_action' => [
        'create_success' => 'Create new question type success',
        'create_fail' => 'Create new question type fail',
        'update_success' => 'Update question type success',
        'update_fail' => 'Update question type room',
        'delete_success' => 'Delete question type success',
        'delete_fail' => 'Delete question type fail',
    ]
];