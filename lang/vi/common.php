<?php

return [
    "btn" => [
        "submit" => "Submit",
        "create" => "Create",
        "update" => "Update",
        "add" => "Add",
        "back" => "Back",
        "search" => "Search"
    ],
    "tag" => [
        "new" => "New",
        "doing" => "Doing",
        "finish" => "Finish"
    ],
    "no_data" => "No data found"
];