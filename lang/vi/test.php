<?php

return [
    'page' => [
        'list' => 'List test',
        'create' => 'Create test',
        'edit' => 'Edit test'
    ],
    'title' => [
        'label' => 'Title',
        'placeholder' => 'Fill the title',
        'validation' => [
            'required' => '',
        ]
    ],
    'test_type' => [
        'label' => 'Test type',
        'text_default' => 'Choose test type',
        'validation' => [
            'required' => '',
        ]
    ],
    'question_type' => [
        'label' => 'Question type',
        'text_default' => 'Choose test question type',
        'validation' => [
            'required' => '',
        ]
    ],
    'question_number' => [
        'label' => 'Num of Question',
        'text_default' => 'Fill number of question',
        'validation' => [
            'required' => '',
        ]
    ],
    'note' => [
        'label' => 'Note',
        'placeholder' => 'Fill the note',
        'validation' => [
            'required' => '',
        ]
    ],
    'total' => [
        'label' => 'Total',
        'placeholder' => 'Fill the total',
        'validation' => [
            'required' => '',
        ]
    ],
    'total_time' => [
        'label' => 'Total time',
        'placeholder' => 'Fill the total time',
        'validation' => [
            'required' => '',
        ]
    ],
    'floor' => [
        'label' => 'Floor',
        'placeholder' => 'Fill the floor',
        'validation' => [
            'required' => '',
        ]
    ],
    'description' => [
        'label' => 'Description',
        'placeholder' => 'Fill the description',
        'validation' => [
            'required' => '',
        ]
    ],
    'start_time' => [
        'label' => 'Start time',
        'placeholder' => 'Fill the start time',
        'validation' => [
            'required' => '',
        ]
    ],
    'finish_time' => [
        'label' => 'End time',
        'placeholder' => 'Fill the end time',
        'validation' => [
            'required' => '',
        ]
    ],
    "status" => [
        "label" => "Status",
        'text_default' => 'Choose status',
        "value" => [
            "on" => "active",
            "off" => "inactive"
        ],
        "validation" => [
            ""
        ]
    ],
    "type" => [
        "label" => "Type",
        'text_default' => 'Choose type',
        "value" => [
            "on" => "Random",
            "off" => "fixed"
        ],
        "validation" => [
            ""
        ]
    ],
    'result_action' => [
        'create_success' => 'Create new test success',
        'create_fail' => 'Create new test fail',
        'update_success' => 'Update test success',
        'update_fail' => 'Update test fail',
        'delete_success' => 'Delete test success',
        'delete_fail' => 'Delete test fail',
    ]
];