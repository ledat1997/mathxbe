<?php

return [
    'page' => [
        'list' => 'List class room',
        'create' => 'Create class room',
        'edit' => 'Edit class room'
    ],
    'name' => [
        'label' => 'Name',
        'placeholder' => 'Fill the name',
        'validation' => [
            'required' => '',
        ]
    ],
    'grade' => [
        'label' => 'Grade',
        'default_value' => 'Fill the Grade',
        'validation' => [
            'required' => '',
        ]
    ],
    'class' => [
        'label' => 'Class',
        'default_value' => 'Fill the Class',
        'validation' => [
            'required' => '',
        ]
    ],
    'teacher' => [
        'label' => 'Teacher',
        'placeholder' => 'Fill the teacher',
        'validation' => [
            'required' => '',
        ]
    ],
    'description' => [
        'label' => 'Description',
        'placeholder' => 'Fill the description',
        'validation' => [
            'required' => '',
        ]
    ],
    'year' => [
        'label' => 'Year',
        'default_value' => 'Fill the year',
        'validation' => [
            'required' => '',
        ]
    ],
    'result_action' => [
        'create_success' => 'Create new class room success',
        'create_fail' => 'Create new class room fail',
        'update_success' => 'Update class room success',
        'update_fail' => 'Update user class room',
        'delete_success' => 'Delete class room success',
        'delete_fail' => 'Delete class room fail',
    ]
];