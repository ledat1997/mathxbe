<?php

return [
    'page' => [
        'list' => 'List question',
        'create' => 'Create question',
        'edit' => 'Edit question'
    ],
    'title' => [
        'label' => 'Title',
        'placeholder' => 'Fill the title',
        'validation' => [
            'required' => '',
        ]
    ],
    'question_type' => [
        'label' => 'Question type',
        'text_default' => 'Choose question type',
        'validation' => [
            'required' => '',
        ]
    ],
    'note' => [
        'label' => 'Note',
        'placeholder' => 'Fill the note',
        'validation' => [
            'required' => '',
        ]
    ],
    "status" => [
        "label" => "Status",
        'text_default' => 'Choose status',
        "value" => [
            "on" => "active",
            "off" => "inactive"
        ],
        "validation" => [
            ""
        ]
    ],
    'result_action' => [
        'create_success' => 'Create new question success',
        'create_fail' => 'Create new question fail',
        'update_success' => 'Update question success',
        'update_fail' => 'Update question fail',
        'delete_success' => 'Delete question success',
        'delete_fail' => 'Delete question fail',
    ]
];