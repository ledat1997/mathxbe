<aside class="main-sidebar sidebar-light-primary elevation-4">

    <a href="#" class="brand-link bg-blue text-center font-weight-bold">
        <span class="brand-text font-weight-light">Math X</span>
    </a>
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                @foreach(config('sidebar') as $side)
                    <li @class(['menu-item', 'menu-open' => Route::is($side['route'])])>
                        <a href="{{ !str_contains($side['route'], '*') ? route($side['route']) : '' }}" @class(['nav-link', 'position-relative', 'active' => Route::is($side['route'])])>
                            <i @class(['nav-icon', $side['icon']])></i>
                            <p>
                                {{ $side['name'] }}
                                @if(isset($side['children']))
                                    <i class="right fas fa-angle-left"></i>
                                @endif
                            </p>
                        </a>
                        @if(isset($side['children']))
                            <ul class="nav nav-treeview">
                                @foreach($side['children'] as $child)
                                    <li class="nav-item">
                                        <a href="{{ route($child['route']) }}" @class(['nav-link', 'active' => Route::is($child['route'])])>
                                            <i @class(['nav-icon', $child['icon']])></i>
                                            <p>{{ $child['name'] }}</p>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach
            </ul>
        </nav>

    </div>
</aside>

@push('script')
    <script>
        document.addEventListener("DOMContentLoaded", function () {
            var menuItems = document.querySelectorAll('.menu-item');
            var menuSimple = Array.from(menuItems).filter(function (item) {
                return !item.querySelector('.nav-treeview');
            });
            menuSimple.forEach(function (item) {
                item.baseURI
            });
        });
    </script>
@endpush