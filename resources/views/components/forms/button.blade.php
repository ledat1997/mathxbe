@props([
    'name' => '',
    'type' => 'button',
    'isLink' => false,
    'link' => ''
])
@if($isLink)
    <a href="{{ $link }}" {{ $attributes->merge(['class' => 'link link-' . $type]) }}>{{ !empty($name) ? $name : $slot }}</a>
@else
    <button
        type="{{ $type }}"
        {{ $attributes->merge(['class' => 'btn btn-' . $type]) }}
    >
        {{ !empty($name) ? $name : $slot }}
    </button>
@endif