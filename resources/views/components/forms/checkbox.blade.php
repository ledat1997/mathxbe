@props([
'label',
'name',
'required' => false,
'wrapperFormInput' => '',
'wrapperInput' => '',
'valueDefault' => '',
'wrapperLabel' => '',
])
<div>
    <div class="form-group {{ $wrapperFormInput }}">
        <label for="checkbox-{{ $name }}" class="{{ $wrapperLabel }} {{ $label ? "" : "d-none" }}">
            {{ $label ?? "" }}
            <span @class(['text-danger', 'd-none' => !$required])>*</span>
        </label>
        <div class="input-wrapper {{ $wrapperInput }}">
            {{ $before ?? '' }}
            <input
                    id="checkbox-{{ $name }}"
                    type="checkbox"
                    {{ $attributes->merge(['class' => 'checkbox-custom']) }}
                    name="{{ $name }}"
                    {{ old($name, $valueDefault) ? 'checked': '' }}
            >
            {{ $after ?? '' }}
            @error($name)
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
</div>