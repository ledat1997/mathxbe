@props([
'label',
'name',
'textOff',
'textOn',
'required' => false,
'wrapperFormInput' => '',
'wrapperInput' => '',
'valueDefault' => '',
'wrapperLabel' => '',
])
<div>
    <div class="form-group {{ $wrapperFormInput }}">
        <label for="checkbox-{{ $name }}" class="{{ $wrapperLabel }}">
            {{ $label }}
            <span @class(['text-danger', 'd-none' => !$required])>*</span>
        </label>
        <div class="input-wrapper {{ $wrapperInput }}">
            {{ $before ?? '' }}
            <input
                id="checkbox-{{ $name }}"
                type="checkbox"
                {{ $attributes->merge(['class' => 'checkbox-switch']) }}
                name="{{ $name }}"
                data-bootstrap-switch
                data-off-text="{{ $textOff }}"
                data-on-text="{{ $textOn }}"
                {{ old($name, $valueDefault) ? 'checked': '' }}
            >
            {{ $after ?? '' }}
            @error($name)
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
</div>