@props([
    'label',
    'name',
    'type' => 'text',
    'placeholder' => '',
    'required' => false,
    'wrapperFormInput' => '',
    'wrapperInput' => '',
    'valueDefault' => '',
    'wrapperLabel' => '',
    'index' => '',
])
@php
    $rand = rand(1,10000);
@endphp
<div class="form-group {{ $wrapperFormInput }}" @if(isset($index)) data-index="{{ $index }}" @endif>
    <label for="input-{{ $name }}-{{ $rand }}" class="{{ $wrapperLabel }} {{ $label ? "" : "d-none" }}">
        {{ $label ?? "" }}
        <span @class(['text-danger', 'd-none' => !$required])>*</span>
    </label>
    <div class="input-wrapper {{ $wrapperInput }}">
        {{ $before ?? '' }}
        <input
                type="{{ $type }}"
                {{ $attributes->merge(['class' => 'form-control input-form']) }}
                name="{{ $name }}"
                id="input-{{ $name }}-{{ $rand }}"
                placeholder="{{ $placeholder }}"
                value="{{ old($name, $valueDefault) }}"
        >
        {{ $after ?? '' }}
        @error($name)
        <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>
    {{ $afterOutInput ?? '' }}
</div>