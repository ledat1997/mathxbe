@props([
'label',
'name',
'dataSelect',
'placeholder' => '',
'required' => false,
'wrapperFormInput' => '',
'wrapperLabel' => '',
'wrapperInput' => '',
'valueDefault' => '',
'textDefault' => '',
'multiple' => ''
])

<div class="form-group {{ $wrapperFormInput }}">
    <label for="selectbox-{{ $name }}" class="{{ $wrapperLabel }}">
        {{ $label }}
        <span @class(['text-danger', 'd-none' => !$required])>*</span>
    </label>
    <div class="input-wrapper {{ $wrapperInput }}">
        <select
            {{ $attributes->merge(['class' => 'form-control select2 rounded-1']) }}
            {{ $multiple ? 'multiple' : '' }}
            name="{{ $multiple ? $name . "[]" : $name }}"
            id="selectbox-{{ $name }}">
            <option value="">{{ $textDefault }}</option>
            @if($multiple)
                @php
                    $valueSelect = !empty(old($name, $valueDefault)) ? old($name, $valueDefault) : [];
                @endphp
                @foreach($dataSelect as $id => $item)
                    <option value="{{ $id }}" {{ in_array($id, $valueSelect) ? 'selected': '' }}>{{ $item }}</option>
                @endforeach
            @else
                @foreach($dataSelect as $id => $item)
                    <option value="{{ $id }}" {{ old($name, $valueDefault) == $id ? 'selected': '' }}>{{ $item }}</option>
                @endforeach
            @endif
        </select>
        @error($name)
        <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>
</div>