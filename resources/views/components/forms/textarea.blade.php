@props([
'label',
'name',
'placeholder' => '',
'required' => false,
'wrapperFormInput' => '',
'wrapperInput' => '',
'valueDefault' => '',
'wrapperLabel' => '',
'row' => 3
])
<div class="form-group {{ $wrapperFormInput }}">
    <label for="textarea-{{ $name }}" class="{{ $wrapperLabel }}">
        {{ $label }}
        <span @class(['text-danger', 'd-none' => !$required])>*</span>
    </label>
    <div class="input-wrapper {{ $wrapperInput }}">
        {{ $before ?? '' }}
        <textarea
            {{ $attributes->merge(['class' => 'form-control']) }}
            rows="{{ $row }}"
            name="{{ $name }}"
            placeholder="{{ $placeholder }}"
            id="textarea-{{ $name }}">{!! old($name, $valueDefault) !!}</textarea>
        {{ $after ?? '' }}
        @error($name)
        <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>
</div>