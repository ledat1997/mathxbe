@props([
    'classWrapper' => '',
    'label',
    'value',
    'icon',
    'bgIcon'
])

<div class="{{ $classWrapper }}">
    <div class="info-box">
        <span class="info-box-icon {{ $bgIcon }}"><i class="{{ $icon }}"></i></span>
        <div class="info-box-content">
            <span class="info-box-text">{{ $label }}</span>
            <span class="info-box-number">{{ $value }}</span>
        </div>
    </div>
</div>