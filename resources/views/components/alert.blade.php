@props([
    'type' => 'success',
    'message' => '',
])
@if(isset($message) && strlen($message))
    <div class="alert alert-{{ $type }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h5><i class="icon fas {{ config('constants.alert.icon.' . $type) }}"></i>{{ $message }}</h5>
    </div>
@endif