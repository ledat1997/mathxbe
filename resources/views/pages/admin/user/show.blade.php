@extends('layouts.main.main_full')
@section('title', trans('user.page.show'))
@push('css')
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">
@endpush

@section('main-content')

    <x-card type="default">
        <x-slot name="header" class="p-4">
            <h3 class="card-title">{{ trans("user.title.user_info") }}</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
            </div>
        </x-slot>
        <x-slot name="body" class="p-4">
            <div class="row">
                <x-box_item classWrapper="col-md-4 col-sm-6 col-12" bgIcon="bg-info" icon="fa-solid fa-chess-pawn" label="Full Name" :value="$user->full_name" />
                <x-box_item classWrapper="col-md-4 col-sm-6 col-12" bgIcon="bg-info" icon="far fa-envelope" label="Email" :value="$user->email" />
                <x-box_item classWrapper="col-md-4 col-sm-6 col-12" bgIcon="bg-info" icon="fa-solid fa-phone" label="Phone" :value="$user->phone" />
                <x-box_item classWrapper="col-md-4 col-sm-6 col-12" bgIcon="bg-info" icon="fa-solid fa-map-location" label="Address" :value="$user->address" />
                <x-box_item classWrapper="col-md-4 col-sm-6 col-12" bgIcon="bg-info" icon="fa-solid fa-circle-user" label="Role" :value="config('constants.user.role.' . $user->role)" />
                <x-box_item classWrapper="col-md-4 col-sm-6 col-12" bgIcon="bg-info" icon="fa-solid fa-triangle-exclamation" label="Status" :value="config('constants.user.status.' . $user->status)" />
            </div>
        </x-slot>
    </x-card>
    @if($user->role == \App\Enums\UserRole::Child)
        <x-card type="default">
            <x-slot name="header" class="p-4">
                <h3 class="card-title">{{ trans("user.title.user_detail_study") }}</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                </div>
            </x-slot>
            <x-slot name="body" class="p-4">
                @php
                    $isAddNew = true;
                @endphp
                @foreach($user->userDetails->sortBy('start_year') as $userDetail)
                    <x-card type="default">
                        <x-slot name="header" class="p-4">
                            <h3 class="card-title">{{ $userDetail->start_year . ' - ' . $userDetail->end_year }}</h3>
                            <div class="card-tools">
                                @if($userDetail->status)
                                    <span class="badge bg-success">{{ trans("common.tag.finish") }}</span>
                                @else
                                    <span class="badge bg-warning">{{ trans("common.tag.doing") }}</span>
                                @endif
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </x-slot>
                        <x-slot name="body" class="p-4">
                            @if($userDetail->status)
                                <x-forms.input label="{{ trans('user.classroom.label') }}" name="" readonly :valueDefault="$userDetail->classRoom->nameTeacher"/>
                                <x-forms.textarea label="{{ trans('user.description.label') }}" name="" readonly :valueDefault="$userDetail->description"/>
                                <x-forms.input label="{{ trans('user.point_average.label') }}" name="" readonly :valueDefault="$userDetail->point_average"/>
                            @else
                                @php
                                    $isAddNew = false;
                                @endphp
                                <form action="{{ route('admin.user_detail.update', $userDetail->id) }}" method="post">
                                    @csrf
                                    @method('put')
                                    <x-forms.selectbox
                                            label="{{ trans('user.classroom.label') }}"
                                            required
                                            name="class_room_id"
                                            :dataSelect="$classroom"
                                            class="input-form"
                                            style="width: 100%"
                                            textDefault="{{ trans('user.classroom.text_default') }}"
                                            :valueDefault="$userDetail->class_room_id"
                                    />
                                    <x-forms.textarea label="{{ trans('user.description.label') }}" name="description" placeholder="{{ trans('user.description.placeholder') }}" :valueDefault="$userDetail->description"/>
                                    <x-forms.input label="{{ trans('user.point_average.label') }}" name readonly :valueDefault="$userDetail->point_average ?? 0" wrapperInput="d-flex">
                                        <x-slot name="after">
                                            <x-forms.button class="btn-success ml-3">
                                                <i class='fas fa-sync-alt'></i>
                                            </x-forms.button>
                                        </x-slot>
                                    </x-forms.input>
                                    <x-forms.input label name="user_id" wrapperFormInput="d-none"="d-none" :valueDefault="$user->id" />
                                    <x-forms.button type="submit" name="submit" class="btn-success d-block mx-auto"/>
                                </form>
                            @endif
                        </x-slot>
                    </x-card>
                @endforeach
                @if($isAddNew)
                    <x-card type="default">
                        <x-slot name="header" class="p-4">
                            <h3 class="card-title">{{ getYear() . ' - ' . getYear() + 1 }}</h3>
                            <div class="card-tools">
                                <span class="badge bg-primary">{{ trans("common.tag.new") }}</span>
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </x-slot>
                        <x-slot name="body" class="p-4">
                            <form action="{{ route('admin.user_detail.store') }}" method="post">
                                @csrf
                                <x-forms.selectbox
                                        label="Class room"
                                        required
                                        name="class_room_id"
                                        :dataSelect="$classroom"
                                        class="input-form"
                                        style="width: 100%"
                                        textDefault="Fill class room"
                                />
                                <x-forms.textarea label="Description" name="description" placeholder="Fill description"/>
                                <x-forms.input label name="user_id" wrapperFormInput="d-none" :valueDefault="$user->id" />
                                <x-forms.button type="submit" name="submit" class="btn-success d-block mx-auto"/>
                            </form>
                        </x-slot>
                    </x-card>
                @endif
            </x-slot>
            <x-slot name="footer" class="p-4 text-center">
                <x-forms.button isLink class="btn btn-default mr-4" :link="route('admin.user.index')" name="{{ trans('common.btn.back') }}"/>
            </x-slot>
        </x-card>
    @elseif($user->role == \App\Enums\UserRole::Parent)
        <x-card type="default">
            <x-slot name="header" class="p-4">
                <h3 class="card-title">{{ trans("user.title.user_children") }}</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                </div>
            </x-slot>
            <x-slot name="body" class="p-4">
                @foreach($user->child as $child)
                    <x-card type="default">
                        <x-slot name="header" class="p-4">
                            <h3 class="card-title">{{ $child->full_name }}</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </x-slot>
                        <x-slot name="body" class="p-4">
                            @forelse($child->userDetails as $childDetail)
                                <x-card type="default">
                                    <x-slot name="header" class="p-4">
                                        <h3 class="card-title">{{ $childDetail->start_year . ' - ' . $childDetail->end_year }}</h3>
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                            </button>
                                        </div>
                                    </x-slot>
                                    <x-slot name="body" class="p-4">
                                        <x-forms.input label="{{ trans('user.classroom.label') }}" name readonly :valueDefault="$childDetail->classRoom->nameTeacher"/>
                                        <x-forms.textarea label="{{ trans('user.description.label') }}" name="" readonly :valueDefault="$childDetail->description"/>
                                        <x-forms.input label="{{ trans('user.point_average.label') }}" name readonly :valueDefault="$childDetail->point_average ?? 0"/>
                                    </x-slot>
                                </x-card>
                            @empty
                                <div>{{ trans('common.no_data') }}</div>
                            @endempty
                        </x-slot>
                    </x-card>
                @endforeach
            </x-slot>
            <x-slot name="footer" class="p-4 text-center">
                <x-forms.button isLink class="btn btn-default mr-4" :link="route('admin.user.index')" name="{{ trans('common.btn.back') }}"/>
            </x-slot>
        </x-card>
    @endif
@endsection
