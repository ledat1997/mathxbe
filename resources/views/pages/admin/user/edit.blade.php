@extends('layouts.main.main_full')
@section('title', trans('user.page.edit'))
@push("css")
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">
@endpush
@section('main-content')
    <div class="row">
        <div class="col-12">
            <form action="{{ route('admin.user.update', $user['id']) }}" method="post">
                @method('put')
                @csrf
                <x-card type="primary">
                    <x-slot name="body" class="p-5">
                        <x-forms.input label="{{ trans('user.full_name.label') }}" name="full_name" placeholder="{{ trans('user.full_name.placeholder') }}" required :valueDefault="$user['full_name']"/>
                        <x-forms.input label="{{ trans('user.email.label') }}" name="email" placeholder="{{ trans('user.email.placeholder') }}" required :valueDefault="$user['email']"/>
                        <x-forms.input label="{{ trans('user.address.label') }}" name="address" placeholder="{{ trans('user.address.placeholder') }}" required :valueDefault="$user['address']"/>
                        <x-forms.input label="{{ trans('user.phone.label') }}" name="phone" placeholder="{{ trans('user.phone.placeholder') }}" required :valueDefault="$user['phone']"/>
                        <x-forms.checkbox_switch class="role-user" label="{{ trans('user.role.label') }}" name="role" required textOff="{{ trans('user.role.value.off') }}" textOn="{{ trans('user.role.value.on') }}" :valueDefault="$user['role'] == 2"/>
                        <x-forms.checkbox_switch label="{{ trans('user.status.label') }}" name="status" required textOff="{{ trans('user.status.value.off') }}" textOn="{{ trans('user.status.value.on') }}" :valueDefault="$user['status']"/>
                        <x-forms.selectbox
                                label="{{ trans('user.user.label') }}"
                                required
                                name="users"
                                :dataSelect="$listChildren"
                                class="input-form select-parent"
                                wrapperFormInput="parent-user {{ old('role', $user['role'] == \App\Enums\UserRole::Parent) ? '' : 'd-none' }}"
                                :valueDefault="$user['child']->toArray()"
                                style="width: 100%"
                                multiple="multiple"
                                id="parent-user"
                        />
                    </x-slot>
                    <x-slot name="footer" class="px-5 text-center">
                        <x-forms.button isLink class="btn btn-default mr-4" :link="route('admin.user.index')" name="{{ trans('common.btn.back') }}"/>
                        <x-forms.button type="submit" class="btn-primary" name="{{ trans('common.btn.update') }}"/>
                    </x-slot>
                </x-card>
            </form>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $("input[data-bootstrap-switch]").each(function(){
            $(this).bootstrapSwitch('state', $(this).prop('checked'));
        })
        $('.role-user').on('switchChange.bootstrapSwitch', function (event, state) {
            if (state) {
                $('.parent-user').removeClass('d-none');
            } else {
                $('.parent-user').addClass('d-none');
                $('#parent-user').select2('val', 'All');
            }
        });
    </script>
@endpush