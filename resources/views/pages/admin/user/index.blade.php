@extends('layouts.main.main_full')
@section('title', trans('user.page.list'))
@push('css')
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">
@endpush

@section('main-content')
    <div class="row">
        <div class="col-12">
            <form action="{{ route('admin.user.index') }}" class="form-search">
                <x-forms.input
                    name="name"
                    label="{{ trans('user.full_name.label') }}"
                    wrapperFormInput="search-input"
                    wrapperInput="wrapper-input-search"
                    wrapperLabel="wrapper-label-search"
                    :valueDefault="request()->name"
                />
                <x-forms.selectbox
                        label="{{ trans('user.status.label') }}"
                        name="status"
                        wrapperFormInput="search-input ml-4"
                        wrapperInput="wrapper-input-search"
                        wrapperLabel="wrapper-label-search"
                        :dataSelect="config('constants.user.status')"
                        :valueDefault="request()->status"
                />
                <x-forms.selectbox
                        label="{{ trans('user.role.label') }}"
                        name="role"
                        wrapperFormInput="search-input ml-4"
                        wrapperInput="wrapper-input-search"
                        wrapperLabel="wrapper-label-search"
                        :dataSelect="config('constants.user.role')"
                        :valueDefault="request()->role"
                />
                <x-forms.button type="submit" class="btn-info ml-4" name="{{ trans('common.btn.search') }}"/>
            </form>
            <div class="card">
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>{{ trans('user.full_name.label') }}</th>
                            <th>{{ trans('user.email.label') }}</th>
                            <th>{{ trans('user.address.label') }}</th>
                            <th>{{ trans('user.phone.label') }}</th>
                            <th>{{ trans('user.role.label') }}</th>
                            <th>{{ trans('user.status.label') }}</th>
                            <th colspan="3" class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($users as $index =>  $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->full_name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->address }}</td>
                                <td>{{ $user->phone }}</td>
                                <td>
                                    <div
                                            @class([
                                                "p-2 badge",
                                                "btn badge-success" => $user->role == \App\Enums\UserRole::Parent,
                                                "badge-warning"=> $user->role == \App\Enums\UserRole::Child
                                            ])
                                        data-toggle="tooltip"
                                        title="{{ $user->child->count() ? getFullNameChild($user->child->pluck('full_name')) : '' }}">
                                        {{ config('constants.user.role.' . $user->role) }}
                                    </div>
                                </td>
                                <td>
                                    <div @class([
                                                "p-2 badge",
                                                "badge-success" => $user->status,
                                                "badge-warning"=> !$user->status
                                            ])>
                                        {{ config('constants.user.status.' . $user->status) }}
                                    </div>
                                </td>
                                <td><a class="btn btn-warning" href="{{ route('admin.user.edit', $user->id) }}"><i class="fa-solid fa-pen-to-square"></i></a></td>
                                <td><a class="btn btn-info" href="{{ route('admin.user.show', $user->id) }}"><i class="fa-solid fa-eye"></i></a></td>
                                <td>
                                    <div type="button" class="btn btn-dark delete-item" data-toggle="modal" data-url="{{ route('admin.user.destroy', $user->id) }}" data-target="#modalDelete">
                                        <i class="fa-solid fa-trash"></i>
                                    </div>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="10" class="text-center">{{ trans('common.no_data') }}</td>
                            </tr>

                        @endforelse
                        </tbody>
                    </table>
                    {{ $users->links('layouts.pagination.index')}}
                </div>
            </div>
        </div>
    </div>
@endsection