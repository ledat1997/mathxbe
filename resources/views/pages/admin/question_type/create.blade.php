@extends('layouts.main.main_full')
@section('title', trans('question_type.page.create'))
@push("css")
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">
@endpush
@section('main-content')
    <div class="row">
        <div class="col-12">
            <form action="{{ route('admin.question_type.store') }}" method="post">
                @csrf
                <x-card type="primary">
                    <x-slot name="body" class="p-5">
                        <x-forms.input label="{{ trans('question_type.title.label') }}" required name="title" placeholder="{{ trans('question_type.title.placeholder') }}"/>
                        <x-forms.textarea label="{{ trans('question_type.description.label') }}" name="description" placeholder="{{ trans('question_type.description.placeholder') }}"/>
                        <x-forms.checkbox_switch label="{{ trans('question_type.status.label') }}" name="status" textOff="{{ trans('question_type.status.value.off') }}" textOn="{{ trans('question_type.status.value.on') }}"/>
                    </x-slot>
                    <x-slot name="footer" class="px-5 text-center">
                        <x-forms.button isLink class="btn btn-default mr-4" :link="route('admin.question_type.index')" name="{{ trans('common.btn.back') }}"/>
                        <x-forms.button type="submit" class="btn-primary" name="{{ trans('common.btn.create') }}"/>
                    </x-slot>
                </x-card>
            </form>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $("input[data-bootstrap-switch]").each(function(){
            $(this).bootstrapSwitch('state', $(this).prop('checked'));
        })
    </script>
@endpush