@extends('layouts.main.main_full')
@section('title', trans('question_type.page.list'))
@push('css')
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">
    <link rel="stylesheet" href="{{ asset('css/table.css') }}">
@endpush

@section('main-content')
    <div class="row">
        <div class="col-12">
            <form action="{{ route('admin.question_type.index') }}" class="form-search">
                <x-forms.input
                        name="title"
                        label="{{ trans('question_type.title.label') }}"
                        wrapperFormInput="search-input"
                        wrapperInput="wrapper-input-search"
                        wrapperLabel="wrapper-label-search"
                        :valueDefault="request()->title"
                />
                <x-forms.selectbox
                        label="{{ trans('question_type.status.label') }}"
                        name="status"
                        wrapperFormInput="search-input ml-4"
                        wrapperInput="wrapper-input-search"
                        wrapperLabel="wrapper-label-search"
                        :dataSelect="config('constants.question_type.status')"
                        :valueDefault="request()->status"
                />
                <x-forms.button type="submit" class="btn-info ml-4" name="{{ trans('common.btn.search') }}"/>
            </form>
            <div class="card">
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>{{ trans('question_type.title.label') }}</th>
                            <th>{{ trans('question_type.description.label') }}</th>
                            <th>{{ trans('question_type.status.label') }}</th>
                            <th colspan="2" class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($questionTypes as $index => $questionType)
                            <tr>
                                <td>{{ $questionType->id }}</td>
                                <td>{{ $questionType->title }}</td>
                                <td><div class="text-too-long">{{ $questionType->description }}</div></td>
                                <td>
                                    <div @class([
                                                "p-2 badge",
                                                "badge-success" => $questionType->status,
                                                "badge-warning"=> !$questionType->status
                                            ])>
                                        {{ config('constants.question_type.status.' . $questionType->status) }}
                                    </div>
                                </td>
                                <td><a class="btn btn-warning" href="{{ route('admin.question_type.edit', $questionType->id) }}"><i class="fa-solid fa-pen-to-square"></i></a></td>
                                <td>
                                    <div type="button" class="btn btn-dark delete-item" data-toggle="modal" data-url="{{ route('admin.question_type.destroy', $questionType->id) }}" data-target="#modalDelete">
                                        <i class="fa-solid fa-trash"></i>
                                    </div>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="9" class="text-center">{{ trans('common.no_data') }}</td>
                            </tr>

                        @endforelse
                        </tbody>
                    </table>
                    {{ $questionTypes->links('layouts.pagination.index')}}
                </div>
            </div>
        </div>
    </div>
@endsection