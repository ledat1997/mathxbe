@extends('layouts.main.main_full')
@section('title', trans('test.page.list'))
@push('css')
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">
    <link rel="stylesheet" href="{{ asset('css/table.css') }}">
@endpush

@section('main-content')
    <div class="row">
        <div class="col-12">
            <form action="{{ route('admin.test.index') }}" class="form-search">
                <x-forms.input
                        name="title"
                        label="{{ trans('test.title.label') }}"
                        placeholder="{{ trans('test.title.placeholder') }}"
                        wrapperFormInput="search-input"
                        wrapperInput="wrapper-input-search"
                        wrapperLabel="wrapper-label-search"
                        :valueDefault="request()->title"
                />
                <x-forms.selectbox
                        label="{{ trans('test.test_type.label') }}"
                        name="test_type"
                        wrapperFormInput="search-test-type ml-4 mb-0"
                        wrapperInput="wrapper-input-search"
                        wrapperLabel="wrapper-label-search"
                        :dataSelect="config('constants.test.test_type')"
                        textDefault="{{ trans('test.test_type.text_default') }}"
                        :valueDefault="request()->test_type"
                />
                <x-forms.selectbox
                        label="{{ trans('test.status.label') }}"
                        name="status"
                        wrapperFormInput="search-input ml-4"
                        wrapperInput="wrapper-input-search"
                        wrapperLabel="wrapper-label-search"
                        :dataSelect="config('constants.test.status')"
                        textDefault="{{ trans('test.status.text_default') }}"
                        :valueDefault="request()->status"
                />
                <x-forms.button type="submit" class="btn-info ml-4" name="{{ trans('common.btn.search') }}"/>
            </form>
            <div class="card">
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>{{ trans('test.title.label') }}</th>
                            <th>{{ trans('test.test_type.label') }}</th>
                            <th>{{ trans('test.total_time.label') }}</th>
                            <th>{{ trans('test.total.label') }}</th>
                            <th>{{ trans('test.floor.label') }}</th>
                            <th>{{ trans('test.status.label') }}</th>
                            <th colspan="2" class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($tests as $index => $test)
                            <tr>
                                <td>{{ $test->id }}</td>
                                <td>{{ $test->title }}</td>
                                <td>{{ config('constants.test.test_type.' . $test->test_type) }}</div></td>
                                <td>{{ $test->total_time }}</td>
                                <td>{{ $test->total }}</td>
                                <td>{{ $test->floor }}</td>
                                <td>
                                    <div @class([
                                                "p-2 badge",
                                                "badge-success" => $test->status,
                                                "badge-warning"=> !$test->status
                                            ])>
                                        {{ config('constants.test.status.' . $test->status) }}
                                    </div>
                                </td>
                                <td><a class="btn btn-warning" href="{{ route('admin.test.edit', $test->id) }}"><i class="fa-solid fa-pen-to-square"></i></a></td>
                                <td>
                                    <div type="button" class="btn btn-dark delete-item" data-toggle="modal" data-url="{{ route('admin.test.destroy', $test->id) }}" data-target="#modalDelete">
                                        <i class="fa-solid fa-trash"></i>
                                    </div>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="9" class="text-center">{{ trans('common.no_data') }}</td>
                            </tr>

                        @endforelse
                        </tbody>
                    </table>
                    {{ $tests->links('layouts.pagination.index')}}
                </div>
            </div>
        </div>
    </div>
@endsection