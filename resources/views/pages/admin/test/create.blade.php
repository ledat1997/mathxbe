@extends('layouts.main.main_full')
@section('title', trans('test.page.create'))
@push("css")
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">
@endpush
@section('main-content')
    <div class="row">
        <div class="col-12">
            <form action="{{ route('admin.test.store') }}" method="post">
                @csrf
                <x-card type="primary">
                    <x-slot name="body" class="p-5">
                        <x-forms.input label="{{ trans('test.title.label') }}" name="title" placeholder="{{ trans('test.title.placeholder') }}" required/>
                        <x-forms.textarea label="{{ trans('test.description.label') }}" name="description" placeholder="{{ trans('test.description.placeholder') }}"/>
                        <div class="row">
                            <div class="col-6">
                                <x-forms.input label="{{ trans('test.total.label') }}" name="total" readonly placeholder="" required/>
                            </div>
                            <div class="col-6">
                                <x-forms.input label="{{ trans('test.floor.label') }}" name="floor" placeholder="{{ trans('test.floor.placeholder') }}" required/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <x-forms.input label="{{ trans('test.total_time.label') }}" name="total_time" placeholder="{{ trans('test.total_time.placeholder') }}" required/>

                            </div>
                            <div class="col-6">
                                <x-forms.input label="{{ trans('test.start_time.label') }}" name="start_time" placeholder="{{ trans('test.start_time.placeholder') }}" required/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <x-forms.input label="{{ trans('test.finish_time.label') }}" name="finish_time" placeholder="{{ trans('test.finish_time.placeholder') }}" required/>
                            </div>
                            <div class="col-6">
                                <x-forms.selectbox
                                        label="{{ trans('test.test_type.label') }}"
                                        required
                                        name="test_type"
                                        :dataSelect="config('constants.test.test_type')"
                                        class="input-form"
                                        style="width: 100%"
                                />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <x-forms.checkbox_switch
                                        label="{{ trans('test.status.label') }}"
                                        name="status"
                                        required
                                        textOff="{{ trans('test.status.value.off') }}"
                                        textOn="{{ trans('test.status.value.on') }}"
                                />
                            </div>
                            <div class="col-6">
                                <x-forms.checkbox_switch
                                        label="{{ trans('test.type.label') }}"
                                        name="type"
                                        required
                                        textOff="{{ trans('test.type.value.off') }}"
                                        textOn="{{ trans('test.type.value.on') }}"
                                />
                            </div>
                        </div>
                        <x-card type="default">
                            <x-slot name="header" class="p-4">
                                <h3 class="card-title">Detail Test</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </x-slot>
                            <x-slot name="body" class="p-4">
                                <div id="wrapper-question-type">
                                    <div class="row item-question-type" data-index="0">
                                        <div class="col-7">
                                            <x-forms.selectbox
                                                    label="{{ trans('test.question_type.label') }}"
                                                    required
                                                    name="questions[0][question_type]"
                                                    :dataSelect="config('constants.test.test_type')"
                                                    class="input-form"
                                                    style="width: 100%"
                                            />
                                        </div>
                                        <div class="col-5">
                                            <x-forms.input
                                                    label="{{ trans('test.question_number.label') }}"
                                                    type="number"
                                                    name="question[0][total]"
                                                    wrapperInput="d-flex"
                                                    index=""
                                                    wrapperFormInput="form-input-test"
                                                    valueDefault=""
                                            >
                                                <x-slot name="after">
                                                    <x-forms.button class="btn-secondary ml-3 remove-question_type" data-index="0" data-question-type-id="0">
                                                        <i class='fas fa-minus'></i>
                                                    </x-forms.button>

                                                </x-slot>
                                            </x-forms.input>
                                        </div>
                                    </div>
                                </div>
                                <x-forms.button class="btn-info add-question-type">
                                    <i class='fas fa-plus'></i>
                                </x-forms.button>
                            </x-slot>
                        </x-card>
                    </x-slot>
                    <x-slot name="footer" class="px-5 text-center">
                        <x-forms.button isLink class="btn btn-default mr-4" :link="route('admin.test.index')" name="{{ trans('common.btn.back') }}"/>
                        <x-forms.button type="submit" class="btn-primary" name="{{ trans('common.btn.create') }}"/>
                    </x-slot>
                </x-card>
            </form>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $("input[data-bootstrap-switch]").each(function(){
            $(this).bootstrapSwitch('state', $(this).prop('checked'));
        })
    </script>
    <script src="{{ asset('js/question_test.js') }}"/>
@endpush