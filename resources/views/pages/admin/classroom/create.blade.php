@extends('layouts.main.main_full')
@section('title', trans('classroom.page.create'))
@push("css")
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">
@endpush
@section('main-content')
    <div class="row">
        <div class="col-12">
            <form action="{{ route('admin.class_room.store') }}" method="post">
                @csrf
                <x-card type="primary">
                    <x-slot name="body" class="p-5">
                        <div class="row">
                            <div class="col-4">
                                <x-forms.selectbox
                                        label="{{ trans('classroom.grade.label') }}"
                                        required
                                        name="grade"
                                        :dataSelect="config('constants.grade')"
                                        class="input-form select-grade"
                                        style="width: 100%"
                                        textDefault="{{ trans('classroom.grade.default_value') }}"
                                />
                            </div>
                            <div class="col-1"></div>
                            <div class="col-4">
                                <x-forms.selectbox
                                        label="{{ trans('classroom.class.label') }}"
                                        required
                                        name="class"
                                        :dataSelect="config('constants.class')"
                                        class="input-form select-class"
                                        style="width: 100%"
                                        textDefault="{{ trans('classroom.class.default_value') }}"
                                />
                            </div>
                        </div>
                        <x-forms.input label="{{ trans('classroom.teacher.label') }}" name="teacher" placeholder="{{ trans('classroom.teacher.placeholder') }}"/>
                        <x-forms.textarea label="{{ trans('classroom.description.label') }}" name="description" placeholder="{{ trans('classroom.teacher.placeholder') }}"/>
                        <x-forms.selectbox
                                label="{{ trans('classroom.year.label') }}"
                                required
                                name="year"
                                :dataSelect="config('constants.year')"
                                class="input-form select-year"
                                style="width: 100%"
                                textDefault="{{ trans('classroom.year.default_value') }}"
                        />
                    </x-slot>
                    <x-slot name="footer" class="px-5 text-center">
                        <x-forms.button isLink class="btn btn-default mr-4" :link="route('admin.class_room.index')" name="{{ trans('common.btn.back') }}"/>
                        <x-forms.button type="submit" class="btn-primary" name="{{ trans('common.btn.create') }}"/>
                    </x-slot>
                </x-card>
            </form>
        </div>
    </div>
@endsection