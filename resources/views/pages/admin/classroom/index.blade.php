@extends('layouts.main.main_full')
@section('title', trans('classroom.page.list'))
@push('css')
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">
    <link rel="stylesheet" href="{{ asset('css/table.css') }}">
@endpush

@section('main-content')
    <div class="row">
        <div class="col-12">
            <form action="{{ route('admin.class_room.index') }}" class="form-search">
                <x-forms.selectbox
                        label="{{ trans('classroom.grade.label') }}"
                        name="grade"
                        wrapperFormInput="search-input"
                        wrapperInput="wrapper-input-search"
                        wrapperLabel="wrapper-label-search"
                        :dataSelect="config('constants.grade')"
                        :valueDefault="request()->grade"
                />
                <x-forms.selectbox
                        label="{{ trans('classroom.class.label') }}"
                        name="class"
                        wrapperFormInput="search-input ml-4"
                        wrapperInput="wrapper-input-search"
                        wrapperLabel="wrapper-label-search"
                        :dataSelect="config('constants.class')"
                        :valueDefault="request()->class"
                />
                <x-forms.input
                        name="teacher"
                        label="{{ trans('classroom.teacher.label') }}"
                        wrapperFormInput="search-input ml-4"
                        wrapperInput="wrapper-input-search"
                        wrapperLabel="wrapper-label-search"
                        :valueDefault="request()->teacher"
                />
                <x-forms.selectbox
                        label="{{ trans('classroom.year.label') }}"
                        name="year"
                        wrapperFormInput="search-input ml-4"
                        wrapperInput="wrapper-input-search"
                        wrapperLabel="wrapper-label-search"
                        :dataSelect="config('constants.year')"
                        :valueDefault="request()->year"
                />
                <x-forms.button type="submit" class="btn-info ml-4" name="{{ trans('common.btn.search') }}"/>
            </form>
            <div class="card">
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>{{ trans('classroom.name.label') }}</th>
                            <th>{{ trans('classroom.teacher.label') }}</th>
                            <th>{{ trans('classroom.description.label') }}</th>
                            <th>{{ trans('classroom.year.label') }}</th>
                            <th colspan="2" class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($classes as $index => $class)
                            <tr>
                                <td>{{ $class->id }}</td>
                                <td>{{ $class->name }}</td>
                                <td>{{ $class->teacher }}</td>
                                <td><div class="text-too-long">{{ $class->description }}</div></td>
                                <td>{{ $class->year }}</td>
                                <td><a class="btn btn-warning" href="{{ route('admin.class_room.edit', $class->id) }}"><i class="fa-solid fa-pen-to-square"></i></a></td>
                                <td>
                                    <div type="button" class="btn btn-dark delete-item" data-toggle="modal" data-url="{{ route('admin.class_room.destroy', $class->id) }}" data-target="#modalDelete">
                                        <i class="fa-solid fa-trash"></i>
                                    </div>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="9" class="text-center">{{ trans('common.no_data') }}</td>
                            </tr>

                        @endforelse
                        </tbody>
                    </table>
                    {{ $classes->links('layouts.pagination.index')}}
                </div>
            </div>
        </div>
    </div>
@endsection