@extends('layouts.main.main_full')
@section('title', trans('question.page.list'))
@push('css')
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">
    <link rel="stylesheet" href="{{ asset('css/table.css') }}">
@endpush

@section('main-content')
    <div class="row">
        <div class="col-12">
            <form action="{{ route('admin.question.index') }}" class="form-search">
                <x-forms.input
                        name="title"
                        label="{{ trans('question.title.label') }}"
                        placeholder="{{ trans('question.title.placeholder') }}"
                        wrapperFormInput="search-input"
                        wrapperInput="wrapper-input-search"
                        wrapperLabel="wrapper-label-search"
                        :valueDefault="request()->title"
                />
                <x-forms.selectbox
                        label="{{ trans('question.question_type.label') }}"
                        name="question_type_id"
                        wrapperFormInput="search-question-type ml-4"
                        wrapperInput="wrapper-input-question-type-search"
                        wrapperLabel="wrapper-label-question-type-search"
                        :dataSelect="$questionTypes"
                        textDefault="{{ trans('question.question_type.text_default') }}"
                        :valueDefault="request()->question_type_id"
                />
                <x-forms.selectbox
                        label="{{ trans('question.status.label') }}"
                        name="status"
                        wrapperFormInput="search-input ml-4"
                        wrapperInput="wrapper-input-search"
                        wrapperLabel="wrapper-label-search"
                        :dataSelect="config('constants.question.status')"
                        textDefault="{{ trans('question.status.text_default') }}"
                        :valueDefault="request()->status"
                />
                <x-forms.button type="submit" class="btn-info ml-4" name="{{ trans('common.btn.search') }}"/>
            </form>
            <div class="card">
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>{{ trans('question.title.label') }}</th>
                            <th>{{ trans('question.question_type.label') }}</th>
                            <th>{{ trans('question.status.label') }}</th>
                            <th colspan="2" class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($questions as $index => $question)
                            <tr>
                                <td>{{ $question->id }}</td>
                                <td>{{ $question->title }}</td>
                                <td>{{ $question->questionType->title }}</div></td>
                                <td>
                                    <div @class([
                                                "p-2 badge",
                                                "badge-success" => $question->status,
                                                "badge-warning"=> !$question->status
                                            ])>
                                        {{ config('constants.question.status.' . $question->status) }}
                                    </div>
                                </td>
                                <td><a class="btn btn-warning" href="{{ route('admin.question.edit', $question->id) }}"><i class="fa-solid fa-pen-to-square"></i></a></td>
                                <td>
                                    <div type="button" class="btn btn-dark delete-item" data-toggle="modal" data-url="{{ route('admin.question.destroy', $question->id) }}" data-target="#modalDelete">
                                        <i class="fa-solid fa-trash"></i>
                                    </div>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="9" class="text-center">{{ trans('common.no_data') }}</td>
                            </tr>

                        @endforelse
                        </tbody>
                    </table>
                    {{ $questions->links('layouts.pagination.index')}}
                </div>
            </div>
        </div>
    </div>
@endsection