@extends('layouts.main.main_full')
@section('title', trans('question.page.create'))
@push("css")
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">
@endpush
@section('main-content')
    <div class="row">
        <div class="col-12">
            <form action="{{ route('admin.question.store') }}" method="post">
                @csrf
                <x-card type="primary">
                    <x-slot name="body" class="p-5">
                        <x-forms.input label="{{ trans('question.title.label') }}" required name="title" placeholder="{{ trans('question.title.placeholder') }}"/>
                        <x-forms.selectbox
                                label="{{ trans('question.question_type.label') }}"
                                required
                                name="question_type_id"
                                :dataSelect="$questionTypes"
                                class="input-form"
                                style="width: 100%"
                                textDefault="{{ trans('question.question_type.text_default') }}"
                        />
                        <x-forms.checkbox_switch label="{{ trans('question.status.label') }}" name="status" textOff="{{ trans('question.status.value.off') }}" textOn="{{ trans('question.status.value.on') }}"/>
                        <x-forms.textarea label="{{ trans('question.note.label') }}" name="note" placeholder="{{ trans('question.note.placeholder') }}"/>
                        <x-card type="default">
                            <x-slot name="header" class="p-4">
                                <h3 class="card-title">Answer Box</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </x-slot>
                            <x-slot name="body" class="p-4">
                                <div id="wrapper-answer">
                                    @forelse(old('answers', []) as $key => $value)
                                        @include('pages.admin.question.includes.form_answer', [
                                            'index' => $key,
                                            'valueInput' => $value['answer'],
                                            'valueCheckbox' => isset($value['status']) ? \App\Enums\AnswerStatus::Correct : \App\Enums\AnswerStatus::Wrong,
                                            'answerId' => ''
                                        ])
                                    @empty
                                        @include('pages.admin.question.includes.form_answer', [
                                            'index' => 0,
                                            'valueInput' => '',
                                            'valueCheckbox' => '',
                                            'answerId' => ''
                                        ])
                                    @endforelse
                                    @error('answers')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <x-forms.button class="btn-info add-answer">
                                    <i class='fas fa-plus'></i>
                                </x-forms.button>
                            </x-slot>
                        </x-card>
                    </x-slot>
                    <x-slot name="footer" class="px-5 text-center">
                        <x-forms.button isLink class="btn btn-default mr-4" :link="route('admin.question.index')" name="{{ trans('common.btn.back') }}"/>
                        <x-forms.button type="submit" class="btn-primary" name="{{ trans('common.btn.create') }}"/>
                    </x-slot>
                </x-card>
            </form>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('js/question_answer.js') }}"></script>
    <script>
        $("input[data-bootstrap-switch]").each(function(){
            $(this).bootstrapSwitch('state', $(this).prop('checked'));
        })
    //    question answer

    </script>
@endpush