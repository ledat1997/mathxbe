
<x-forms.input
    label=""
    name="answers[{{ $index }}][answer]"
    wrapperInput="d-flex w-100"
    index="{{ $index }}"
    wrapperFormInput="form-input-answer"
    valueDefault="{{ $valueInput }}"
>
    <x-slot name="after">
        <x-forms.checkbox
                label="correct"
                name="answers[{{ $index }}][status]"
                wrapperLabel="label-checkbox-answer"
                wrapperFormInput="mb-0 ml-3"
                class="checkbox-answer"
                valueDefault="{{ $valueCheckbox }}"
        />
        <x-forms.input
                label=""
                name="answers[{{ $index }}][id]"
                wrapperFormInput="d-none"
                valueDefault="{{ $answerId }}"
        />

        <x-forms.button class="btn-secondary ml-3 remove-answer" data-index="{{ $index }}" data-answer-id="{{ $answerId }}">
            <i class='fas fa-minus'></i>
        </x-forms.button>

    </x-slot>
    <x-slot name="afterOutInput">
        @error("answers.{$index}.answer")
        <div class="text-danger">{{ $message }}</div>
        @enderror
    </x-slot>
</x-forms.input>