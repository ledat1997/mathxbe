$('.add-answer').on('click', function (){
    let wrapperAnswer = document.getElementById('wrapper-answer');
    let answer = wrapperAnswer.querySelector('.form-input-answer:last-child');
    let index = parseInt(answer.getAttribute('data-index')) + 1;
    let newAnswer = answer.cloneNode(true);
    newAnswer.querySelectorAll('input').forEach(input => {
        if (input.type == "checkbox") {
            input.checked = false;
        } else {
            input.value = '';
        }
        let name = input.getAttribute('name');
        let newName = name.replace(/\[\d+\]/, `[${index}]`);
        input.setAttribute('name', newName);
    });
    newAnswer.querySelector('.remove-answer').setAttribute('data-index', index);
    newAnswer.querySelector('.remove-answer').setAttribute('data-answer-id', "");
    newAnswer.setAttribute('data-index', index);

    wrapperAnswer.appendChild(newAnswer);
    updateRemoveButtons(newAnswer);
})

function updateRemoveButtons(answer) {
    const removeButton = answer.querySelector('.remove-answer');
    if (removeButton) {
        removeButton.addEventListener('click', function() {
            let indexRemove = parseInt(removeButton.getAttribute('data-index'));
            if (!isNaN(indexRemove)) {
                let answerIdRemove = parseInt(removeButton.getAttribute('data-answer-id'));
                let wrapperAnswer = document.getElementById('wrapper-answer');
                let answerRemove = wrapperAnswer.querySelector(`.form-input-answer[data-index="${indexRemove}"]`);

                if (answerRemove && wrapperAnswer.childElementCount > 1) {
                    answerRemove.remove();
                    if (!isNaN(answerIdRemove)) {
                        let answerDelete = $(".answer-delete").val();
                        addAnswerDelete = answerDelete + "," + answerIdRemove;
                        $(".answer-delete").val(addAnswerDelete)
                    }

                    let currentIndex = 0;
                    wrapperAnswer.querySelectorAll('.form-input-answer').forEach(answer => {
                        answer.setAttribute('data-index', currentIndex);
                        answer.querySelectorAll('input').forEach(input => {
                            let name = input.getAttribute('name');
                            let newName = name.replace(/\[\d+\]/, `[${currentIndex}]`);
                            input.setAttribute('name', newName);
                        });
                        answer.querySelectorAll('.remove-answer').forEach(removeAnswer => {
                            removeAnswer.setAttribute('data-index', currentIndex);
                        });
                        currentIndex++;
                    });
                }
            }
        });
    }
}
let wrapperAnswer = document.getElementById('wrapper-answer');
wrapperAnswer.querySelectorAll('.form-input-answer').forEach(answer => {
    updateRemoveButtons(answer);
});