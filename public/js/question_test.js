// $('.add-question-type').on('click', function (){
//     let wrapperQuestionType = document.getElementById('wrapper-question-type');
//     let questionType = wrapperQuestionType.querySelector('.item-question-type:last-child');
//     let index = parseInt(questionType.getAttribute('data-index')) + 1;
//     $(questionType).find('select.select2').select2('destroy');
//     let newQuestionType = questionType.cloneNode(true);
//     newQuestionType.querySelectorAll('input').forEach(input => {
//         input.value = '';
//         let name = input.getAttribute('name');
//         let newName = name.replace(/\[\d+\]/, `[${index}]`);
//         input.setAttribute('name', newName);
//     });
//     newQuestionType.querySelectorAll('select').forEach(select => {
//         select.value = '';
//         let name = select.getAttribute('name');
//         let newName = name.replace(/\[\d+\]/, `[${index}]`);
//         select.setAttribute('name', newName);
//         select.setAttribute('id', 'selectbox-' + newName);
//         select.removeAttribute('data-select2-id');
//         $(select).select2();
//     });
//     newQuestionType.querySelector('.remove-question_type').setAttribute('data-index', index);
//     newQuestionType.querySelector('.remove-question_type').setAttribute('data-answer-id', "");
//     newQuestionType.setAttribute('data-index', index);
//
//     wrapperQuestionType.appendChild(newQuestionType);
//     updateRemoveButtons(newQuestionType);
// })
//
// function updateRemoveButtons(questionType) {
//     const removeButton = questionType.querySelector('.remove-question_type');
//     if (removeButton) {
//         removeButton.addEventListener('click', function() {
//             let indexRemove = parseInt(removeButton.getAttribute('data-index'));
//             if (!isNaN(indexRemove)) {
//                 let questionTypeIdRemove = parseInt(removeButton.getAttribute('data-question-type-id'));
//                 let wrapperQuestionType = document.getElementById('wrapper-question-type');
//                 let questionTypeRemove = wrapperQuestionType.querySelector(`.item-question-type[data-index="${indexRemove}"]`);
//
//                 if (questionTypeRemove && wrapperQuestionType.childElementCount > 1) {
//                     questionTypeRemove.remove();
// //                     if (!isNaN(questionTypeIdRemove)) {
// //                         let answerDelete = $(".answer-delete").val();
// //                         addAnswerDelete = answerDelete + "," + answerIdRemove;
// //                         $(".answer-delete").val(addAnswerDelete)
// //                     }
//
//                     let currentIndex = 0;
//                     wrapperQuestionType.querySelectorAll('.item-question-type').forEach(questionType => {
//                         questionType.setAttribute('data-index', currentIndex);
//                         questionType.querySelectorAll('input').forEach(input => {
//                             let name = input.getAttribute('name');
//                             let newName = name.replace(/\[\d+\]/, `[${currentIndex}]`);
//                             input.setAttribute('name', newName);
//                         });
//                         questionType.querySelectorAll('.remove-answer').forEach(removeQuestionType => {
//                             removeQuestionType.setAttribute('data-index', currentIndex);
//                         });
//                         currentIndex++;
//                     });
//                 }
//             }
//         });
//     }
// }
// let wrapperQuestionType = document.getElementById('wrapper-question-type');
// wrapperQuestionType.querySelectorAll('.item-question-type').forEach(questionType => {
//     updateRemoveButtons(questionType);
// });