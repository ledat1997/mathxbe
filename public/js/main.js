$('.menu-item').on('click', function() {
    $(this).toggleClass("menu-open")
    if ($(this).hasClass("menu-open")) {
        $(this).find('.nav-treeview').removeClass('d-none')
    } else {
        $(this).find('.nav-treeview').addClass('d-none')
    }
})


$('.delete-item').on("click", function(){
    var urlDelete = $(this).data('url');
    console.log(urlDelete);
    $('.form-modal-delete').attr('action', urlDelete);
})