<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('questions', function (Blueprint $table) {
            $table->json('images')->nullable()->after('status');
            $table->text('title')->change();
            $table->tinyInteger('answer_correct')->after('question_type_id')->default(1)->comment('number of correct answers to the question');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('questions', function (Blueprint $table) {
            $table->dropColumn(['images', 'answer_correct']);
            $table->string('title')->change();;
        });
    }
};
