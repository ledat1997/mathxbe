<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('user_details', function (Blueprint $table) {
            $table->dropColumn('start_date');
            $table->string('start_year', 4)->after('status');
            $table->string('end_year', 4)->after('start_year');
            $table->double('point_average')->nullable()->after('end_year');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('user_details', function (Blueprint $table) {
            $table->string('start_date');
            $table->dropColumn(['start_year', 'end_year', 'point_average']);
        });
    }
};
