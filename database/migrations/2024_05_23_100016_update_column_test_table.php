<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tests', function (Blueprint $table) {
            $table->tinyInteger('test_type')->after('detail')->comment('1 base test, 2 hour test 3 midterm test 4: final test');
            $table->tinyInteger('time_test')->default(1);
            $table->tinyInteger('type')->comment('1: fixed, 2: random')->default(1);
            $table->dateTime('start_time')->change();
            $table->dateTime('finish_time')->change();
            $table->tinyInteger('status')->comment('0:inactive, 1 :active');
            $table->dropColumn(['detail', 'class_test']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tests', function (Blueprint $table) {
            $table->dropColumn(['test_type', 'time_test', 'status', 'type']);
            $table->string('start_time', 20)->change();
            $table->string('finish_time', 20)->change();
            $table->json('class_test');
            $table->json('detail');
        });
    }
};
