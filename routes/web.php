<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\QuestionController;
use App\Http\Controllers\Admin\ClassRoomController;
use App\Http\Controllers\Admin\TestController;
use App\Http\Controllers\Admin\UserTestController;
use App\Http\Controllers\Admin\QuestionTypeController;
use App\Http\Controllers\Admin\UserDetailController;

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::group(['as' => 'auth.', 'middleware' => 'auth_logged'], function () {
        Route::get('/login', [AuthController::class, 'formLogin'])->name('login.form');
        Route::post('/login', [AuthController::class, 'login'])->name('login.submit');
    });
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
        Route::get('/profile', [AuthController::class, 'profile'])->name('user.profile');

        // user
        Route::resource('user', UserController::class);
        Route::resource('question', QuestionController::class);
        Route::resource('class-room', ClassRoomController::class)->names('class_room');
        Route::resource('test', TestController::class);
        Route::resource('user-test', UserTestController::class)->names('user_test');
        Route::resource('question-type', QuestionTypeController::class)->names('question_type');

        Route::group(['prefix' => 'user-detail', 'as' => 'user_detail.'], function (){
            Route::post('/', [UserDetailController::class, 'store'])->name('store');
            Route::put('/update/{user_detail}', [UserDetailController::class, 'update'])->name('update');
        });
    });
});
